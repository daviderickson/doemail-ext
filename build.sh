#!/bin/bash
version=`cat install.rdf | grep "em:version" | sed "s;.*version>\(.*\)</em:version.*;\1;"`
archive="doemail-${version}.xpi"
rm -rf ${archive}
zip -x .\* \*.xpi build.sh Makefile eclipseCodeStyle.xml temp/\* image_source/\* todo.txt \*/.svn/\* \*/.git/\* @ -r ${archive} *

