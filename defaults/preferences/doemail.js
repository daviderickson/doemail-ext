pref("extensions.doemail.doemailRoot", "https://www.doemail.org/");
pref("extensions.doemail.doemailUsername", "");
pref("extensions.doemail.doemailPassword", "");
pref("extensions.doemail.doemailDelimiter", "+");
pref("extensions.doemail.doemailDebug", false);
pref("extensions.doemail.lastDeleteQuery", 0);
pref("mailnews.customDBHeaders", "x-doemail x-doemail-score");