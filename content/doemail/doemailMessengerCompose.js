function messengerComposeIconsUpdateEventHandler(event) {
	// Update all icons
	composeWindowFocus();
}

function composeWindowFocus() {
	// on load we may have messed up values here
	var i = 1;
	var ele = document.getElementById("addressCol2#" + i.toString());
	while (ele) {
		messengerComposeAddressCheck(ele);
		i++;
		ele = document.getElementById("addressCol2#" + i.toString());
	}
}

function messengerComposeAddressCheck(targetNode) {
	var regexResults = emailRegex.exec(targetNode.value);
	if (regexResults != null) {
		var encodedData = "email=" + encodeURIComponent(json_encode([regexResults[0]]));
		doemailRequest("checkEmailAddress", encodedData, messengerComposeAddressCallback, {target: targetNode});
		//targetNode.childNodes[1].attributes[0].value = "address-status-unknown";
	} else {
		targetNode.childNodes[1].attributes[0].value = "";
	}
}

function messengerComposeAddressCallback(data, options) {
	var exitCode = data.exitCode;
	var message = data.message;

	if (exitCode == 0) {
		options.target.childNodes[1].attributes[0].value = "address-status-" + message;
	}
}

function messengerComposeAddressFocus(targetNode) {
	// If this node was just cloned, make sure its graphic is empty
	if ((targetNode.childNodes.length > 1) && targetNode.childNodes[1].attributes.length > 0) {
		targetNode.childNodes[1].attributes[0].value = "";
	}
}
