window.addEventListener("load", initUpdateWindow, false);

function initUpdateWindow() {
	var currentVersion = window.arguments[0];
	var newVersion = window.arguments[1];
	var currentVersionLabel = document.getElementById("currentVersionLabel");
	var newVersionLabel = document.getElementById("newVersionLabel");
	currentVersionLabel.value = currentVersion;
	newVersionLabel.value = newVersion;
	window.setFocus();
}

function openAddonsUpdateWindow() {
	var wm = Components.classes["@mozilla.org/appshell/window-mediator;1"]
										 .getService(Components.interfaces.nsIWindowMediator);
	var enumerator = wm.getEnumerator(null);
	// open the add-ons window
	while(enumerator.hasMoreElements()) {
		var win = enumerator.getNext();
		if (win.document.getElementById("messengerWindow") != null) {
			win.openAddonsMgr();
			break;
		}
	}

	// search for updates
	/**
	var calledUpdate = false;
	while(!calledUpdate) {
		enumerator = wm.getEnumerator(null);
		while(enumerator.hasMoreElements()) {
			var win = enumerator.getNext();
			if (win.document.getElementById("extensionsManager") != null) {
				win.checkUpdatesAll();
				calledUpdate = true;
				break;
			}
		}
	}
	**/

	// close our window
	window.close();
}