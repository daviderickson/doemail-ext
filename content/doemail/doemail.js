var doemail_version = "0.0.29";
var emailRegex = /(((\"[^\"\f\n\r\t\v\b]+\")|([\w\!\#\$\%\&\'\*\+\-\~\/\^\`\|\{\}]+(\.[\w\!\#\$\%\&\'\*\+\-\~\/\^\`\|\{\}]+)*))@((\[(((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9])))\])|(((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9])))|((([A-Za-z0-9\-])+\.)+[A-Za-z\-]+)))/;
var emailRegexGlobal = /(((\"[^\"\f\n\r\t\v\b]+\")|([\w\!\#\$\%\&\'\*\+\-\~\/\^\`\|\{\}]+(\.[\w\!\#\$\%\&\'\*\+\-\~\/\^\`\|\{\}]+)*))@((\[(((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9])))\])|(((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9]))\.((25[0-5])|(2[0-4][0-9])|([0-1]?[0-9]?[0-9])))|((([A-Za-z0-9\-])+\.)+[A-Za-z\-]+)))/g;
var domainRegex = /(^[A-Za-z0-9\-]+(\.[A-Za-z0-9\-]+)*$)/;
var pendingEmailTempFileName = "doemail_pending_temp";

function doemailCheckForUpdate() {
	doemailRequest("", "", doemailCheckForUpdateCallback, {url: "extension_version_tb24.php"});
}

function doemailCheckForUpdateCallback(data, options) {
	var myVersionArray = doemail_version.split(".");
	var remoteVersionArray = data.split(".");
	var updateNeeded = false;
	for (index in remoteVersionArray) {
		var myVersion = Number(myVersionArray[index]);
		var remoteVersion = Number(remoteVersionArray[index]);
		if (remoteVersion > myVersion) {
			updateNeeded = true;
		}
	}

	if (updateNeeded) {
		window.openDialog("chrome://doemail/content/updateWindow.xul","doemailUpdateWindow","chrome,resizable=no", doemail_version, data);
	}
}

function menuAddToWhitelist(emailAddressNode) {
	var address = emailAddressNode.getAttribute("emailAddress");
	addWhitelistEmail([address], genericStatusAndAlertCallback, {});
	//doemailRequest("addWhitelistEmail", encodedData, genericStatusAndAlertCallback);
}

function menuAddToWhitelistDomain(emailAddressNode) {
	var address = emailAddressNode.getAttribute("emailAddress");
	address = address.substring(address.indexOf("@") + 1, address.length);
	//var encodedData = "domain=" + encodeURIComponent(address);
	//doemailRequest("addWhitelistDomain", encodedData, genericStatusAndAlertCallback);
	addWhitelistDomain([address], genericStatusAndAlertCallback, {});
}

function menuAddToBlacklist(emailAddressNode) {
	var address = emailAddressNode.getAttribute("emailAddress");
	//var encodedData = "email=" + encodeURIComponent(address);
	//doemailRequest("addBlacklistEmail", encodedData, genericStatusAndAlertCallback);
	addBlacklistEmail([address], genericStatusAndAlertCallback, {});
}

function menuAddToBlacklistDomain(emailAddressNode) {
	var address = emailAddressNode.getAttribute("emailAddress");
	address = address.substring(address.indexOf("@") + 1, address.length);
	//var encodedData = "domain=" + encodeURIComponent(address);
	//doemailRequest("addBlacklistDomain", encodedData, genericStatusAndAlertCallback);
	addBlacklistDomain([address], genericStatusAndAlertCallback, {});
}

function selectedTextEmailAddress(toWhitelist) {
	var selectedText = getSelectedText();
	if (selectedText.length > 4) {
		// TODO regex email matching
		email = selectedText;
		if (toWhitelist) {
			addWhitelistEmail([email], genericStatusAndAlertCallback, {});
			//doemailRequest("addWhitelistEmail", encodedData, genericStatusAndAlertCallback);
		} else {
			addBlacklistEmail([email], genericStatusAndAlertCallback, {});
			//doemailRequest("addBlacklistEmail", encodedData, genericStatusAndAlertCallback);
		}
	}
}

function selectedTextDomain(toWhitelist) {
	var selectedText = getSelectedText();
	if (selectedText.length > 4) {
		// TODO regex email matching
		//var encodedData = "domain=" + encodeURIComponent(selectedText);
		if (toWhitelist) {
			//doemailRequest("addWhitelistDomain", encodedData, genericStatusAndAlertCallback);
			addWhitelistDomain([selectedText], genericStatusAndAlertCallback, {});
		} else {
			//doemailRequest("addBlacklistDomain", encodedData, genericStatusAndAlertCallback);
			addBlacklistDomain([selectedText], genericStatusAndAlertCallback, {});
		}
	}
}

// -----------------------------------------------------
// FUNCTIONS TO ADD EMAIL AND DOMAINS
// -----------------------------------------------------

function addWhitelistEmail(email, callback, options) {
	if (email instanceof Array) {
		for (key in email) {
			if (("object" != typeof(email[key])) || (email[key] instanceof String)) {
				// we found an old incoming type, convert to the new object type for transmission
				var newArray = new Array();

				for (key2 in email) {
					var obj = new Object();
					obj.email = email[key2];
					newArray.push(obj);
				}
				email = newArray;
				break;
			}
		}
	}
	
	var encodedData="email=" + encodeURIComponent(json_encode(email));
	if (options.events) {
		options.events = options.events + ",doemail_dirty";
	} else {
		options.events = "doemail_dirty";
	}
	doemailRequest("addWhitelistEmail", encodedData, callback, options);
}

function addWhitelistDomain(domain, callback, options) {
	var encodedData="domain=" + encodeURIComponent(json_encode(domain));
	if (options.events) {
		options.events = options.events + ",doemail_dirty";
	} else {
		options.events = "doemail_dirty";
	}
	doemailRequest("addWhitelistDomain", encodedData, callback, options);
}

function addBlacklistEmail(email, callback, options) {
	var encodedData="email=" + encodeURIComponent(json_encode(email));
	if (options.events) {
		options.events = options.events + ",doemail_dirty";
	} else {
		options.events = "doemail_dirty";
	}
	doemailRequest("addBlacklistEmail", encodedData, callback, options);
}

function addBlacklistDomain(domain, callback, options) {
	var encodedData="domain=" + encodeURIComponent(json_encode(domain));
	if (options.events) {
		options.events = options.events + ",doemail_dirty";
	} else {
		options.events = "doemail_dirty";
	}
	doemailRequest("addBlacklistDomain", encodedData, callback, options);
}

function addWhitelistTo(tovalue, callback, options) {
	var encodedData="tovalue=" + encodeURIComponent(json_encode(tovalue));
	if (options.events) {
		options.events = options.events + ",doemail_dirty";
	} else {
		options.events = "doemail_dirty";
	}
	doemailRequest("addWhitelistTo", encodedData, callback, options);
}

// -----------------------------------------------------
// FUNCTIONS TO REMOVE EMAIL AND DOMAINS
// -----------------------------------------------------

function removeWhitelistEmail(email, callback, options) {
	var encodedData = "email=" + encodeURIComponent(json_encode(email));
	if (options.events) {
		options.events = options.events + ",doemail_dirty";
	} else {
		options.events = "doemail_dirty";
	}
	doemailRequest("removeWhitelistEmail", encodedData, callback, options);
}

function removeBlacklistEmail(email, callback, options) {
	var encodedData = "email=" + encodeURIComponent(json_encode(email));
	if (options.events) {
		options.events = options.events + ",doemail_dirty";
	} else {
		options.events = "doemail_dirty";
	}
	doemailRequest("removeBlacklistEmail", encodedData, callback, options);
}

function removeWhitelistDomain(domain, callback, options) {
	var encodedData = "domain=" + encodeURIComponent(json_encode(domain));
	if (options.events) {
		options.events = options.events + ",doemail_dirty";
	} else {
		options.events = "doemail_dirty";
	}
	doemailRequest("removeWhitelistDomain", encodedData, callback, options);
}

function removeBlacklistDomain(domain, callback, options) {
	var encodedData = "domain=" + encodeURIComponent(json_encode(domain));
	if (options.events) {
		options.events = options.events + ",doemail_dirty";
	} else {
		options.events = "doemail_dirty";
	}
	doemailRequest("removeBlacklistDomain", encodedData, callback, options);
}

function removeWhitelistTo(tovalue, callback, options) {
	var encodedData = "tovalue=" + encodeURIComponent(json_encode(tovalue));
	if (options.events) {
		options.events = options.events + ",doemail_dirty";
	} else {
		options.events = "doemail_dirty";
	}
	doemailRequest("removeWhitelistTo", encodedData, callback, options);
}

// -----------------------------------------------------
// ADD/REMOVE CODES
// -----------------------------------------------------

/*
 * Params:
 * code_objects: array of type Object, with 2 properties each, code and description
 * callback: callback function when this http request returns
 * options: usual set of options
 */
function addCode(code_objects, callback, options) {
	var encodedData="data=" + encodeURIComponent(json_encode(code_objects));
	if (options.events) {
		options.events = options.events + ",doemail_dirty";
	} else {
		options.events = "doemail_dirty";
	}
	doemailRequest("addCode", encodedData, callback, options);
}

function removeCode(code, callback, options) {
	var encodedData = "code=" + encodeURIComponent(json_encode(code));
	if (options.events) {
		options.events = options.events + ",doemail_dirty";
	} else {
		options.events = "doemail_dirty";
	}
	doemailRequest("removeCode", encodedData, callback, options);
}

// -----------------------------------------------------
// MISC
// -----------------------------------------------------

function approveEmailSenders(senders, callback, options) {
	var encodedData = "senders=" + encodeURIComponent(json_encode(senders));
	doemailRequest("approveEmailSenders", encodedData, callback, options);
}

function approveEmailDomains(domains, callback, options) {
	var encodedData = "domains=" + encodeURIComponent(json_encode(domains));
	doemailRequest("approveEmailDomains", encodedData, callback, options);
}

function approveSpecificEmail(senderFilePairs, callback, options) {
	var encodedData = "senderFilePairs=" + encodeURIComponent(json_encode(senderFilePairs));
	doemailRequest("approveSpecificEmail", encodedData, callback, options);
}


function removeEmailBySenders(senders, callback, options) {
	var encodedData = "senders=" + encodeURIComponent(json_encode(senders));
	doemailRequest("removeEmailBySenders", encodedData, callback, options);
}

function removeEmailByDomains(domains, callback, options) {
	var encodedData = "domains=" + encodeURIComponent(json_encode(domains));
	doemailRequest("removeEmailByDomains", encodedData, callback, options);
}

function removeSpecificEmail(senderFilePairs, callback, options) {
	var encodedData = "senderFilePairs=" + encodeURIComponent(json_encode(senderFilePairs));
	doemailRequest("removeSpecificEmail", encodedData, callback, options);
}

// -----------------------------------------------------
// Queries
// -----------------------------------------------------

function queryEmailDelete(rows) {
	var encodedData = "rows=" + encodeURIComponent(json_encode(rows));
	doemailRequest("queryEmailDelete", encodedData, genericStatusAndAlertCallback, {});
}

function showDoemailWindow(event) {
	// Ensure its a left click on the button to trigger the window opening
	if (!(event instanceof MouseEvent) || event.button != 0) {
		return;
	}

	var doemailWin = getDoemailWindow();
	if (doemailWin == null) {
		var myWindow = window.open("chrome://doemail/content/doemailWindow.xul","doemailWindow","chrome,resizable=yes");
	} else {
		doemailWin.doemailRefreshAllData();
		doemailWin.focus();
	}
}

function genericStatusAndAlertCallback(data) {
	var exitCode = data.exitCode;
	var message = data.message;

	var statusText = document.getElementById("statusText");
	if (exitCode == 0) {
		statusText.setAttribute("label", message);
	} else {
		//statusText.setAttribute("label", message);
		// clear the status bar
		statusText.setAttribute("label", "");
		//alert(message);
	}
}

function checkResponseForError(xmlResponse) {
	var results = xmlResponse.getElementsByTagName("exitCode");
	if ((results.length > 0) && (parseInt(results.item(0).firstChild.nodeValue) == 1)) {
		return xmlResponse.getElementsByTagName("message").item(0).firstChild.nodeValue;
	}

	return null;
}

function doemailGetUsername() {
	var prefManager = Components.classes["@mozilla.org/preferences-service;1"]
                                .getService(Components.interfaces.nsIPrefBranch);
	return prefManager.getCharPref("extensions.doemail.doemailUsername");
}

function doemailRequest(command, encodedData, callback, options) {
	var prefManager = Components.classes["@mozilla.org/preferences-service;1"]
                                .getService(Components.interfaces.nsIPrefBranch);
	var doemailRoot = prefManager.getCharPref("extensions.doemail.doemailRoot");
	var doemailUsername = prefManager.getCharPref("extensions.doemail.doemailUsername");
	var doemailPassword = prefManager.getCharPref("extensions.doemail.doemailPassword");
	doemailPassword = SHA256(doemailPassword);
	var doemailDebug = prefManager.getBoolPref("extensions.doemail.doemailDebug");

	if (options.hasOwnProperty("username")) {
		doemailUsername = options.username;
	}

	if (options.hasOwnProperty("password")) {
		doemailPassword = options.password;
	}

	var request = new XMLHttpRequest();

	var url = doemailRoot;

	if (options.hasOwnProperty("url")) {
		url += options.url;
	} else {
		url += "service.php";
	}

	if (doemailDebug) {
		url = url + "?DBGSESSID=" + Math.floor(Math.random()*1000) + ";";

		if (options.hasOwnProperty("debug") && options.debug) {
			url = url + "d%3D1";
		} else {
			url = url + "d%3D0";
		}
	}

	// extract the host header
	var hostHeader;
	if (url.indexOf("https://") != -1) {
		hostHeader = url.split("https://")[1];
		hostHeader = hostHeader.split("/")[0];
	} else if (url.indexOf("http://") != -1) {
		hostHeader = url.split("http://")[1];
		hostHeader = hostHeader.split("/")[0];
	}

	request.open("POST", url);

	// build POST String
	var data = "username=" + encodeURIComponent(doemailUsername) + "&password=" +
		encodeURIComponent(doemailPassword) + "&command=" + encodeURIComponent(command);

	if (options.transaction) {
		data += "&transaction=" + encodeURIComponent(options.transaction);
	}

	data += "&" + encodedData;

	//dump("Post Data: " + data + "\n");
	request.postData = data;
	request.setRequestHeader("Host", hostHeader);
	request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	request.setRequestHeader("Content-length", data.length);
	request.setRequestHeader("Connection", "close");


	request.aborted = false;

	request.timeoutFunc = function timeout() {
		request.aborted = true;
		if (options.hasOwnProperty("stopProgressMeter") && (options.stopProgressMeter != false)) {
			options.stopProgressMeter();
		} else {
			stopProgressMeter();
		}

		if (options.timeoutCallback) {
			options.timeoutCallback(options);
		}
		request.abort();
		setStatusText("DOEmail: Failure contacting server, please try again later.");
	}

	request.onreadystatechange = function() {
		if (request.readyState == 4) {
			if (request.aborted) {
			  // nothing really to do here
			} else if (request.status != 200) {
				try {
					setDoemailStatusText("DOEmail: Error " + request.status + ": " + request.statusText);
				} catch (err) {
					// handle XML network errors
					setDOEmailStatusTest("DOEmail: Error " + err.name + "-" + err.message);
				}
			} else {
				var respText = request.responseText;
				var data = json_decode(respText);
				//var xmlData = request.responseXML;
				if (callback) {
					callback(data, options);
				}

				// trigger any events this call requires
				if (options.events) {
					var event_array = options.events.split(",");
					for (index in event_array) {
						var win = getThunderbirdMessengerWindow();
						var event = win.document.createEvent('Events');
						event.initEvent(event_array[index], false, true);
						win.dispatchEvent(event);
					}
				}
			}

			if (options.hasOwnProperty("stopProgressMeter")) {
				if (options.stopProgressMeter == false) {
				// dont stop the meter
				} else {
					// this is a custom stop function to stop a progress meter
					options.stopProgressMeter();
				}
			} else {
				stopProgressMeter();
			}

			if (!request.aborted) {
				clearTimeout(request.timeoutTimer);
			}
		}
	}

	request.onerror = function onError(e) {
		try {
			setDoemailStatusText("DOEmail: Error " + e.target.status + " occurred while receiving the document.");
		} catch (err) {
			// handle XML network errors
			setDOEmailStatusTest("DOEmail: Error " + err.name + "-" + err.message);
		}
	}



	if (options.timeout) {
		request.timeoutTimer = setTimeout(request.timeoutFunc, options.timeout);
	} else {
		request.timeoutTimer = setTimeout(request.timeoutFunc, 120000);
	}
	request.send(data);

	// check for custom progress meter to start
	if (options.hasOwnProperty("startProgressMeter")) {
		options.startProgressMeter();
	} else {
	  // start default DOEmail window progress meter
		startProgressMeter();
	}

	/* unless specified, clear the status text on the window */
	if (!options.hasOwnProperty("clearStatusText")) {
		clearStatusText();
	}
}

function getThunderbirdMessengerWindow() {
	var wm = Components.classes["@mozilla.org/appshell/window-mediator;1"]
										 .getService(Components.interfaces.nsIWindowMediator);
	var enumerator = wm.getEnumerator(null);
	while(enumerator.hasMoreElements()) {
		var win = enumerator.getNext();
		if (win.document.getElementById("messengerWindow") != null) {
			return win;
		}
	}

	return null;
}

function getDoemailWindow() {
	var wm = Components.classes["@mozilla.org/appshell/window-mediator;1"]
										 .getService(Components.interfaces.nsIWindowMediator);
	var enumerator = wm.getEnumerator(null);
	while(enumerator.hasMoreElements()) {
		var win = enumerator.getNext();
		if (win.document.getElementById("doemailWindow") != null) {
			return win;
		}
	}

	return null;
}

function getDoemailWindowDocument() {
	var wm = Components.classes["@mozilla.org/appshell/window-mediator;1"]
										 .getService(Components.interfaces.nsIWindowMediator);
	var enumerator = wm.getEnumerator(null);
	while(enumerator.hasMoreElements()) {
		var win = enumerator.getNext();
		if (win.document.getElementById("doemailWindow") != null) {
			return win.document;
		}
	}

	return null;
}

function startProgressMeter() {
	var doc = getDoemailWindowDocument();
	if (doc != null) {
		var pm = doc.getElementById("doemailProgress");
		pm.mode = "undetermined";
	}
}

function stopProgressMeter() {
	var doc = getDoemailWindowDocument();
	if (doc != null) {
		var pm = doc.getElementById("doemailProgress");
		pm.mode = "determined";
		pm.value = 0;
	}
}

function setDoemailStatusText(text) {
	var doc = getDoemailWindowDocument();
	if (doc != null) {
		var statusBar = doc.getElementById("statusText");
		statusBar.label = text;
	}
}

function checkPopupMenuItems(popupNode) {
	var children = popupNode.childNodes;
	for (var i = 0; i < children.length; i++) {
		var anonid = children[i].getAttribute("anonid");
		if (anonid == "doemail") {

		}
	}
}

function getSelectedText() {
	var node = document.popupNode;
	var selection = "";

	/*
	 * This case occurs when clicking an email address above an email in the
	 * preview frame in the main pane of Tbird. It is triggered by the
	 * emailAddressPopup menu.
	 */
	if (node.nodeName == "xul:label" && node.parentNode &&
			node.parentNode.parentNode && 
			node.parentNode.parentNode.nodeName == "mail-emailaddress") {
		selection = node.parentNode.parentNode.getAttribute("emailAddress");

	// check if this was an address in the header of a window
	}	else if (node.nodeName == "mail-emailaddress") {
		selection = node.getAttribute("emailAddress");

	// check if right clicked on a mailto link in a message window
	}	else if (node.protocol && ("mailto:" == node.protocol)) {
		selection = node.href.substring(7, node.href.length);

	// check if we right clicked the icon in the compose window
	} else if (node.nodeName == "image") {
		selection = node.parentNode.value.toLowerCase();

	// check if we right clicked in the compose window's text input field
	} else if (node.parentNode && node.parentNode.parentNode && node.parentNode.parentNode.parentNode && (node.parentNode.parentNode.parentNode.id.indexOf("addressCol2") != -1)) {
		selection = node.value;
		
	} else {
		var nodeLocalName = node.localName.toUpperCase();
		if ((nodeLocalName == "TEXTAREA") || (nodeLocalName == "INPUT" && node.type == "text")) {
				selection = node.value.substring(node.selectionStart, node.selectionEnd);
		}
		else {
				var focusedWindow = document.commandDispatcher.focusedWindow;
				selection = focusedWindow.getSelection().toString();
		}
	}

	return selection;
}

function collapseElement(element) {
    element.style.visibility = "collapse";
}

function uncollapseElement(element) {
    element.style.visibility = "";
}

function updateUndoRedoMenu() {
    //XXX give Lightning some undo/redo UI!
}

function doemailIsOffline() {
	var ioService = Components.classes["@mozilla.org/network/io-service;1"]
										 .getService(Components.interfaces.nsIIOService);

	return ioService.offline;
}

function newTreeView() {
	var treeView = {
		rowCount: 0,
		selection: null,
		data: null,
		sortedColumn: null,
		sortedDirection: 0,
		columnDecorator: null,
		
		// BEGIN FUNCTIONS
		//canDrop: function canDrop(index, orientation, dataTransfer) {return false;},
		cycleCell: function cycleCell(row, col) {
			this.data[row][col.id] = !this.data[row][col.id];
		},
		cycleHeader: function cycleHeader(column) {
			// clear old column's sort
			if (this.sortedColumn) {
				this.sortedColumn.element.setAttribute("sortDirection", "");
			}

			// are we swapping an existing sorted column?
			if (column == this.sortedColumn) {
				if (this.sortedDirection == 0) {
					this.sortedDirection = 1;
				} else {
					this.sortedDirection = 0;
				}
			} else {
				this.sortedColumn = column;
				this.sortedDirection = 0;
			}
			this.sortData();
		},
		//drop: function drop(row, orientation, dataTransfer) {},
		getCellProperties: function getCellProperties(row,col,props){},
		getCellText: function getCellText(row,column){
			if (this.columnDecorator != null) {
				if (this.columnDecorator.hasOwnProperty(column.id)) {
					return this.columnDecorator[column.id](row, column, this.data);
				}
			}
			return this.data[row][column.id];
		},
//		getCellValue: function getCellValue(row, column) {
//			return this.data[row][column.id];
//		},
		getColumnProperties: function getColumnProperties(colid,col,props){},
		getImageSrc: function getImageSrc(row,col){
			return "";
			// DO NOT return anything that is not an image source in chrome, 
			// it will kill performance because it will do a file lookup on
			// every single value returned here
			//return this.data[row][col.id];
		},
		getLevel: function getLevel(row){ return 0; },
		getParentIndex: function getParentIndex(rowIndex) {return -1;},
//		getProgressMode: function getProgressMode(row,col) {
//			return this.data[row][col.id];
//		},
		getRowProperties: function getRowProperties(row,props){},
		hasNextSibling: function hasNextSibling(rowIndex, afterIndex) {
			if ((afterIndex+1) >= this.data.length) {
				return false;
			} else {
				return true;
			}
		},
		isContainer: function isContainer(row) { return false; },
		isContainerEmpty: function isContainerEmpty(index) { return false; },
		isContainerOpen: function isContainerOpen(index) { return false; },
//		isEditable: function isEditable(row, col) {
//			if (col.element.hasAttribute("editable")) {
//				return col.element.getAttribute("editable");
//			} else {
//				return false;
//			}
//		},
//		isSelectable: function isSelectable(row, col) {return true;},
		isSeparator: function isSeparator(row){ return false; },
		isSorted: function isSorted(){ return false; },
//		performAction: function performAction(action) {},
//		performActionOnRow: function performActionOnRow(action, row) {},
//		performActionOnCell: function performActionOnRow(action, row, col) {},
//		selectionChanged: function selectionChanged() {},
//		setCellText: function setCellText(row, column, text) {
//			this.data[row][column.id] = text;
//			this.treebox.invalidateCell(row, column);
//		},
//		setCellValue: function setCellValue(row, column, value) {
//			if (value == "true") {
//				this.data[row][column.id] = true;
//			} else {
//				this.data[row][column.id] = false;
//			}
//			this.treebox.invalidateCell(row, column);
//		},
		setData: function setData(data) { this.data = data; },
		setRowCount: function setRowCount(rowCount) { this.rowCount = rowCount; },
		setTree: function setTree(treebox) { this.treebox = treebox; },
		toggleOpenState: function toggleOpenState(index) {},
		sortData: function sortData() {
			var sortProp = this.sortedColumn.id;

			function ascStringSort(a, b) {
				return (a[sortProp] < b[sortProp]) ? -1 : (a[sortProp] > b[sortProp]) ? 1 : 0;
			}

			function descStringSort(a, b) {
				return (a[sortProp] > b[sortProp]) ? -1 : (a[sortProp] < b[sortProp]) ? 1 : 0;
			}

			if (this.sortedDirection == 0) {
				this.data.sort(ascStringSort);
				this.sortedColumn.element.setAttribute("sortDirection", "ascending");
			} else {
				this.data.sort(descStringSort);
				this.sortedColumn.element.setAttribute("sortDirection", "descending");
			}
		}
	};

	return treeView;
}

function clearStatusText() {
	var statusBar = document.getElementById("statusText");
	if (statusBar != null) {
		statusBar.label = "";
	}
}

function setStatusText(text) {
	var statusBar = document.getElementById("statusText");
	if (statusBar != null) {
		statusBar.label = text;
	}
}

function doemailTestLogin() {
	var doemailUsername = document.getElementById("doemailUsernameField").value;
	if (doemailUsername.indexOf("@") == -1) {
		doemailUsername += "@doemail.org";
		var ele = document.getElementById("doemailUsernameField");
		ele.value = doemailUsername;
		ele = document.getElementById("doemailUsername");
		ele.value = doemailUsername;
	}
	var doemailPassword = document.getElementById("doemailPasswordField").value;
	doemailRequest("authenticationTest", "", doemailTestLoginCallback, {username: doemailUsername,
																																			password: SHA256(doemailPassword),
																																			timeoutCallback: doemailTestLoginTimeout,
																																			timeout: 10000});
}

function doemailTestLoginTimeout(options) {
	var strbundle = document.getElementById("doemailStringBundle");
	alert(strbundle.getString("doemail.preferences.general.testTimeout"));
}

function doemailTestLoginCallback(data, options) {
	var exitCode = data.exitCode;
	var message = data.message;
	var strbundle = document.getElementById("doemailStringBundle");

	if (exitCode == 0) {
		alert(strbundle.getString("doemail.preferences.general.loginTestSuccess"));
	} else {
		alert(strbundle.getString("doemail.preferences.general.loginTestFailure"));
	}
}

function getUniqueDomains(emailAddressArray) {
	var domains = new Array();
	for (index in emailAddressArray) {
		if (domains.indexOf(emailAddressArray[index]) == -1) {
			domains.push(emailAddressArray[index].substring(emailAddressArray[index].indexOf("@")+1, emailAddressArray[index].length));
		}
	}
	return domains;
}

/**
*
* Secure Hash Algorithm (SHA256)
* http://www.webtoolkit.info/
*
* Original code by Angel Marin, Paul Johnston.
*
**/

function SHA256(s){

    var chrsz = 8;
    var hexcase = 0;

    function safe_add (x, y) {
        var lsw = (x & 0xFFFF) + (y & 0xFFFF);
        var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
        return (msw << 16) | (lsw & 0xFFFF);
    }

    function S (X, n) { return ( X >>> n ) | (X << (32 - n)); }
    function R (X, n) { return ( X >>> n ); }
    function Ch(x, y, z) { return ((x & y) ^ ((~x) & z)); }
    function Maj(x, y, z) { return ((x & y) ^ (x & z) ^ (y & z)); }
    function Sigma0256(x) { return (S(x, 2) ^ S(x, 13) ^ S(x, 22)); }
    function Sigma1256(x) { return (S(x, 6) ^ S(x, 11) ^ S(x, 25)); }
    function Gamma0256(x) { return (S(x, 7) ^ S(x, 18) ^ R(x, 3)); }
    function Gamma1256(x) { return (S(x, 17) ^ S(x, 19) ^ R(x, 10)); }

    function core_sha256 (m, l) {
        var K = new Array(0x428A2F98, 0x71374491, 0xB5C0FBCF, 0xE9B5DBA5, 0x3956C25B, 0x59F111F1, 0x923F82A4, 0xAB1C5ED5, 0xD807AA98, 0x12835B01, 0x243185BE, 0x550C7DC3, 0x72BE5D74, 0x80DEB1FE, 0x9BDC06A7, 0xC19BF174, 0xE49B69C1, 0xEFBE4786, 0xFC19DC6, 0x240CA1CC, 0x2DE92C6F, 0x4A7484AA, 0x5CB0A9DC, 0x76F988DA, 0x983E5152, 0xA831C66D, 0xB00327C8, 0xBF597FC7, 0xC6E00BF3, 0xD5A79147, 0x6CA6351, 0x14292967, 0x27B70A85, 0x2E1B2138, 0x4D2C6DFC, 0x53380D13, 0x650A7354, 0x766A0ABB, 0x81C2C92E, 0x92722C85, 0xA2BFE8A1, 0xA81A664B, 0xC24B8B70, 0xC76C51A3, 0xD192E819, 0xD6990624, 0xF40E3585, 0x106AA070, 0x19A4C116, 0x1E376C08, 0x2748774C, 0x34B0BCB5, 0x391C0CB3, 0x4ED8AA4A, 0x5B9CCA4F, 0x682E6FF3, 0x748F82EE, 0x78A5636F, 0x84C87814, 0x8CC70208, 0x90BEFFFA, 0xA4506CEB, 0xBEF9A3F7, 0xC67178F2);
        var HASH = new Array(0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A, 0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19);
        var W = new Array(64);
        var a, b, c, d, e, f, g, h, i, j;
        var T1, T2;

        m[l >> 5] |= 0x80 << (24 - l % 32);
        m[((l + 64 >> 9) << 4) + 15] = l;

        for ( var i = 0; i<m.length; i+=16 ) {
            a = HASH[0];
            b = HASH[1];
            c = HASH[2];
            d = HASH[3];
            e = HASH[4];
            f = HASH[5];
            g = HASH[6];
            h = HASH[7];

            for ( var j = 0; j<64; j++) {
                if (j < 16) W[j] = m[j + i];
                else W[j] = safe_add(safe_add(safe_add(Gamma1256(W[j - 2]), W[j - 7]), Gamma0256(W[j - 15])), W[j - 16]);

                T1 = safe_add(safe_add(safe_add(safe_add(h, Sigma1256(e)), Ch(e, f, g)), K[j]), W[j]);
                T2 = safe_add(Sigma0256(a), Maj(a, b, c));

                h = g;
                g = f;
                f = e;
                e = safe_add(d, T1);
                d = c;
                c = b;
                b = a;
                a = safe_add(T1, T2);
            }

            HASH[0] = safe_add(a, HASH[0]);
            HASH[1] = safe_add(b, HASH[1]);
            HASH[2] = safe_add(c, HASH[2]);
            HASH[3] = safe_add(d, HASH[3]);
            HASH[4] = safe_add(e, HASH[4]);
            HASH[5] = safe_add(f, HASH[5]);
            HASH[6] = safe_add(g, HASH[6]);
            HASH[7] = safe_add(h, HASH[7]);
        }
        return HASH;
    }

    function str2binb (str) {
        var bin = Array();
        var mask = (1 << chrsz) - 1;
        for(var i = 0; i < str.length * chrsz; i += chrsz) {
            bin[i>>5] |= (str.charCodeAt(i / chrsz) & mask) << (24 - i%32);
        }
        return bin;
    }

    function Utf8Encode(string) {
        string = string.replace(/\r\n/g,"\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {

            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }

        }

        return utftext;
    }

    function binb2hex (binarray) {
        var hex_tab = hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
        var str = "";
        for(var i = 0; i < binarray.length * 4; i++) {
            str += hex_tab.charAt((binarray[i>>2] >> ((3 - i%4)*8+4)) & 0xF) +
            hex_tab.charAt((binarray[i>>2] >> ((3 - i%4)*8 )) & 0xF);
        }
        return str;
    }

    s = Utf8Encode(s);
    return binb2hex(core_sha256(str2binb(s), s.length * chrsz));
}
