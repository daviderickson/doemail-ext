/*
	Author: David Erickson
	Date: 04/28/07
	This code was adapted from the reference at www.json.org
	Reason being, augmenting the Object and Array prototypes
	is not feasible or functional under Thunderbird 2.0,
	thus necessitating a function based implementation
	of encoding and decoding JSON.
*/

function json_encode(obj) {

	// function to specifically encode and work on objects
	function object_encode(o) {
		if (o instanceof Date) {
			function f(n) {
				// Format integers to have at least two digits.
				return n < 10 ? '0' + n : n;
			}

			return '"' + o.getFullYear() + '-' +
							f(o.getMonth() + 1) + '-' +
							f(o.getDate()) + 'T' +
							f(o.getHours()) + ':' +
							f(o.getMinutes()) + ':' +
							f(o.getSeconds()) + '"';

		} else if (o instanceof Array) {
			var a = ['['],  // The array holding the text fragments.
					b,          // A boolean indicating that a comma is required.
					i,          // Loop counter.
					l = o.length,
					v;          // The value to be stringified.

			function p(s) {
				// p accumulates text fragments in an array. It inserts a comma before all
				// except the first fragment.

				if (b) {
					a.push(',');
				}
				a.push(s);
				b = true;
			}

			// For each value in this array...
			for (i = 0; i < l; i += 1) {
				v = o[i];
				switch (typeof v) {
				// Serialize a JavaScript object value. Ignore objects thats lack the
				// toJSONString method. Due to a specification error in ECMAScript,
				// typeof null is 'object', so watch out for that case.

					case 'object':
						if (v) {
							p(json_encode(v));
						} else {
							p(json_encode("null"));
						}
						break;

					case 'string':
					case 'number':
					case 'boolean':
						p(json_encode(v));
						break;
				}
			}

			// Join all of the fragments together and return.
			a.push(']');
			return a.join('');
		} else {
			var a = ['{'],  // The array holding the text fragments.
					b,          // A boolean indicating that a comma is required.
					k,          // The current key.
					v;          // The current value.

			function p(s) {
				// p accumulates text fragment pairs in an array. It inserts a comma before all
				// except the first fragment pair.
				if (b) {
					a.push(',');
				}
				a.push(json_encode(k), ':', s);
				b = true;
			}

			// Iterate through all of the keys in the object, ignoring the proto chain.
			for (k in o) {
				if (o.hasOwnProperty(k)) {
					v = o[k];
					switch (typeof v) {
					// Due to a specification error in ECMAScript,
					// typeof null is 'object', so watch out for that case.
						case 'object':
							if (v) {
								p(json_encode(v));
							} else {
								p("null");
							}
							break;
						case 'string':
						case 'number':
						case 'boolean':
								p(json_encode(v));
					}
				}
			}

			// Join all of the fragments together and return.
			a.push('}');
			return a.join('');
		}
	}

	switch (typeof obj) {
		case 'number':
			return isFinite(obj) ? String(obj) : "null";
			break;
		case 'boolean':
			return String(obj);
			break;
		case 'string':
			// m is a table of character substitutions.
			var m = {
					'\b': '\\b',
					'\t': '\\t',
					'\n': '\\n',
					'\f': '\\f',
					'\r': '\\r',
					'"' : '\\"',
					'\\': '\\\\'
			};
			if (/["\\\x00-\x1f]/.test(obj)) {
					return '"' + obj.replace(/([\x00-\x1f\\"])/g, function (a, b) {
							var c = m[b];
							if (c) {
									return c;
							}
							c = b.charCodeAt();
							return '\\u00' +
									Math.floor(c / 16).toString(16) +
									(c % 16).toString(16);
					}) + '"';
			}
			return '"' + obj + '"';
			break;
		case 'object':
			return object_encode(obj);
			break;
		default:
			break;
	}
}

function json_decode(obj, filter) {
	var m = {
			'\b': '\\b',
			'\t': '\\t',
			'\n': '\\n',
			'\f': '\\f',
			'\r': '\\r',
			'"' : '\\"',
			'\\': '\\\\'
	};

	// Parsing happens in three stages. In the first stage, we run the text against
	// a regular expression which looks for non-JSON characters. We are especially
	// concerned with '()' and 'new' because they can cause invocation, and '='
	// because it can cause mutation. But just to be safe, we will reject all
	// unexpected characters.

	var j;

	function walk(k, v) {
		var i;
		if (v && typeof v === 'object') {
			for (i in v) {
				if (v.hasOwnProperty(i)) {
					v[i] = walk(i, v[i]);
				}
			}
		}
		return filter(k, v);
	}

	try {
			if (/^("(\\.|[^"\\\n\r])*?"|[,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t])+?$/.
							test(obj)) {

	// In the second stage we use the eval function to compile the text into a
	// JavaScript structure. The '{' operator is subject to a syntactic ambiguity
	// in JavaScript: it can begin a block or an object literal. We wrap the text
	// in parens to eliminate the ambiguity.

					j = eval('(' + obj + ')');

	// In the optional third stage, we recursively walk the new structure, passing
	// each name/value pair to a filter function for possible transformation.

					if (typeof filter === 'function') {
							j = walk('', j);
					}
					return j;
			}
	} catch (e) {

	// Fall through if the regexp test fails.

	}
	throw new SyntaxError("parseJSON");
}
