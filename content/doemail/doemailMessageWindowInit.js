window.addEventListener("load", initMessageWindow, false);

function initMessageWindow() {
	var messagePaneContextPopup = document.getElementById("messagePaneContext");
	messagePaneContextPopup.addEventListener("popupshowing", menuShowingEvent, false);
	messagePaneContextPopup.addEventListener("popuphiding", menuHidingEvent, false);

	var emailAddressPopup = document.getElementById("emailAddressPopup");
	emailAddressPopup.addEventListener("popupshowing", menuShowingEvent, false);
	emailAddressPopup.addEventListener("popuphiding", menuHidingEvent, false);
}