window.addEventListener("load", initMessageComposeWindow, false);
window.addEventListener("focus", composeWindowFocus, false);

function initMessageComposeWindow() {
	var msgComposeContext = document.getElementById("msgComposeContext");
	msgComposeContext.addEventListener("popupshowing", menuShowingEvent, false);
	msgComposeContext.addEventListener("popuphiding", menuHidingEvent, false);

	var iconContext = document.getElementById("doemailIconContext");
	iconContext.addEventListener("popupshowing", menuShowingEvent, false);
	iconContext.addEventListener("popuphiding", menuHidingEvent, false);

	var tbirdMainWindow = getThunderbirdMessengerWindow();
	tbirdMainWindow.addEventListener("doemail_dirty", messengerComposeIconsUpdateEventHandler, true);
	window.addEventListener("close",
		function (event) {
			tbirdMainWindow.removeEventListener("doemail_dirty", messengerComposeIconsUpdateEventHandler, true);
			return true;
		}, false);
}