var gData = Array();
var gDataCurrent = Array();
var gBareAddresses = Array();
var gBareAddressesToRow = Array();
var gTreeView = null;
var gGen = getAddressesFromEmailURIsGenerator();

function massImportWindowLoad() {
	var tree = document.getElementById("massImportTree");
	gTreeView = newTreeView();

	//gData.push({checked: true, email: "test", domain: "test", username: "test"});
	gTreeView.setData(gDataCurrent);
	gTreeView.setRowCount(gDataCurrent.length);

	var sortedCol = tree.columns.getSortedColumn();
	if (sortedCol != null) {
		gTreeView.sortedColumn = sortedCol;
		if ((sortedCol.element.getAttribute("sortedColumn") != null) &&
				("descending" == sortedCol.element.getAttribute("sortDirection"))) {
			gTreeView.sortedDirection = 1;
		} else {
			gTreeView.sortedDirection = 0;
		}
		gTreeView.sortData();
	}

	tree.view = gTreeView;
	tree.treeBoxObject.invalidate();

	//window.setTimeout(getAddressesFromEmailURIs, 250);
	window.setTimeout(driveGenerator, 250);
}

function driveGenerator() {
	if (gGen.next()) {
		window.setTimeout(driveGenerator, 0);
	} else {
		gGen.close();
		var tree = document.getElementById("massImportTree");
		gTreeView.sortData();
		tree.treeBoxObject.invalidate();
		stopLocalProgressMeter();
	}
	updateEmailAddressCount();
}

function extractEmailAddresses(text) {
	var regexResults = Array();
	var recipientsArray = text.split(",");
	for (var j = 0; j < recipientsArray.length; ++j) {
		if (j in recipientsArray) {
			var recipientString = recipientsArray[j];
			var result = null;
			emailRegexGlobal.lastIndex = 0;
			if ((result = emailRegexGlobal.exec(recipientString)) != null) {
				// email address is in result[0]
				// remove the email from the source string
				recipientString.replace(result[0], "");
				// did the source string contain <?
				var pos;
				var name = "";
				if ((pos = recipientString.indexOf("<")) > 0) {
					name = recipientString.slice(0, pos);
					name = name.replace(/["']/g, "");
					name = name.replace(/^[\s]*/g, "");
				} else if ((pos = recipientString.indexOf("(")) > 0) {
					name = recipientString.slice(pos+1, recipientString.lastIndexOf(")"));
				}

				regexResults = regexResults.concat({email: result[0], name: name});
			}
		}
	}
	return regexResults;
}

function getAddressesFromEmailURIsGenerator() {
	var uriArray = window.arguments[0];
	var messenger = window.arguments[1];
	var tree = document.getElementById("massImportTree");

	var i = 0;
	while (i < uriArray.length) {
		if (i in uriArray)  {
			var uri = uriArray[i];
			var msgService = messenger.messageServiceFromURI(uri);
			var hdr = msgService.messageURIToMsgHdr(uri);

			var regexResults = Array();
			if (hdr.author != null) {
				regexResults = regexResults.concat(extractEmailAddresses(hdr.author));
			}

			if (hdr.recipients != null) {
				regexResults = regexResults.concat(extractEmailAddresses(hdr.recipients));
			}

			var index = gDataCurrent.length;
			var count = 0;

			regexResults.forEach(function regexResultsIterate(element, index, array) {
				// use indexOf on the bare addresses as it doesn't work for object references, just strings
				var email = element.email.toLowerCase();
				var name = element.name;
				if (gBareAddresses.indexOf(email) == -1) {
					gBareAddresses.push(email);

					var row = {selected: true,
						email: email,
						domain: email.substring(email.indexOf("@")+1, email.length),
						username: email.substring(0, email.indexOf("@")),
						count: 1,
						name: name};

					gBareAddressesToRow[email] = row;
					gData.push(row);
					gDataCurrent.push(row);
					++count;
				} else {
					// increment count
					gBareAddressesToRow[email].count += 1;
					if ((gBareAddressesToRow[email].name.length == 0) && (name.length > 0)) {
						gBareAddressesToRow[email].name = name;
					}
				}
			});

			if (count > 0) {
				//alert("index: " + index + " count: " + count);
				gTreeView.setRowCount(gDataCurrent.length);
				tree.treeBoxObject.rowCountChanged(index, count);
				//tree.treeBoxObject.invalidate();
			}

			++i;
			if ((i % 50) == 0) {
				yield true;
			}
		}
	}

	yield false;
}

function onSelectAll() {
	gDataCurrent.forEach(function (element, index, array) {
		element.selected = true;
	});
	gTreeView.treebox.invalidate();
}

function onSelectNone() {
	gDataCurrent.forEach(function (element, index, array) {
		element.selected = false;
	});
	gTreeView.treebox.invalidate();
}

function onAdd() {
	var doemailMenuList = document.getElementById("doemailMenuList");
	if (doemailMenuList.selectedItem.value == "none") {
		var strbundle = document.getElementById("doemailStringBundle");
		alert(strbundle.getString("doemail.massImport.add.mustChooseListWarning"));
		return;
	}

	// build the array of selected elements
	var selected = Array();
	gDataCurrent.forEach(function (element, index, array) {
		if (element.selected) {
			selected.push(element.email);
		}
	});

	switch (doemailMenuList.selectedItem.value) {
		case "whitelistEmail":
			addWhitelistEmail(selected, genericStatusAndAlertCallback,
			{ startProgressMeter: startLocalProgressMeter,
				stopProgressMeter: stopLocalProgressMeter
			});
			break;
		case "whitelistDomain":
			var domains = getUniqueDomains(selected);
			addWhitelistDomain(domains, genericStatusAndAlertCallback,
			{ startProgressMeter: startLocalProgressMeter,
				stopProgressMeter: stopLocalProgressMeter
			});
			break;
		case "blacklistEmail":
			addBlacklistEmail(selected, genericStatusAndAlertCallback,
			{ startProgressMeter: startLocalProgressMeter,
				stopProgressMeter: stopLocalProgressMeter
			});
			break;
		case "blacklistDomain":
			var domains = getUniqueDomains(selected);
			addBlacklistDomain(domains, genericStatusAndAlertCallback,
			{ startProgressMeter: startLocalProgressMeter,
				stopProgressMeter: stopLocalProgressMeter
			});
			break;
		case "whitelistTo" :
			addWhitelistTo(selected, genericStatusAndAlertCallback,
			{ startProgressMeter: startLocalProgressMeter,
				stopProgressMeter: stopLocalProgressMeter
			});
			break;
	}
}

function startLocalProgressMeter() {
	var progressMeter = document.getElementById("progressMeter");
	progressMeter.mode = "undetermined";
}

function stopLocalProgressMeter() {
	var progressMeter = document.getElementById("progressMeter");
	progressMeter.mode = "determined";
	progressMeter.value = 0;
}

function onClearEmailAddressSearchInput() {
	var ele = document.getElementById("emailAddressSearch");
	ele.value = "";
	onEmailAddressSearchInput();
}

function onEmailAddressSearchInput() {
	var val = document.getElementById("emailAddressSearch").value;
	var previousLength = gDataCurrent.length;

	gDataCurrent.splice(0, gDataCurrent.length);

	if ((val == null) || (val == "")) {
		gData.forEach(function (element, index, array) {
			gDataCurrent.push(element);
		});
	} else {
		gData.forEach(function (element, index, array) {
			if (element.email.indexOf(val, 0) >= 0) {
				gDataCurrent.push(element);
			} else {

				// Search the Name too
				var lName = element.name.toLowerCase();
				var lVal = val.toLowerCase();
				if (lName.indexOf(lVal, 0) >= 0) {
					gDataCurrent.push(element);
				}
			}
		});
	}

	var tree = document.getElementById("massImportTree");
	gTreeView.setRowCount(gDataCurrent.length);
	gTreeView.sortData();
	tree.treeBoxObject.rowCountChanged(0, -previousLength);
	tree.treeBoxObject.rowCountChanged(0, gDataCurrent.length);
	//tree.treeBoxObject.invalidate();
	updateEmailAddressCount();
}

function onRemoveSelected() {
	var index = gDataCurrent.length;
	var count = 0;
	var indicesToRemove = Array();
	var tree = document.getElementById("massImportTree");

	gDataCurrent.forEach(function (element, index, array) {
		if (element.selected) {
			indicesToRemove.push(index);
			gData.splice(gData.indexOf(element), 1);
		}
	});
	indicesToRemove.reverse();

	indicesToRemove.forEach(function (element, index, array) {
		gDataCurrent.splice(element, 1);
		gTreeView.setRowCount(gDataCurrent.length);
		tree.treeBoxObject.rowCountChanged(element, -1);
	});
	updateEmailAddressCount();
}

function updateEmailAddressCount() {
	var ele = document.getElementById("emailAddressCount");
	var strbundle = document.getElementById("doemailStringBundle");
  ele.value = strbundle.getString("doemail.massImport.emailAddresses.label") + " " + gDataCurrent.length;
}