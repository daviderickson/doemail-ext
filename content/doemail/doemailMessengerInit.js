window.addEventListener("load", initMessenger, false);

var oldMsgDeleteMessage;

function initMessenger() {
	// Can we determine the OS version we are running under?
	var appInfo =
		Components.classes["@mozilla.org/xre/app-info;1"].getService(Components.interfaces.nsIXULAppInfo);
	if (appInfo instanceof Components.interfaces.nsIXULRuntime) {
		var smallButtonId = "doemailToolbarButton";
		var largeButtonId = "doemailToolbarButtonOSX";

		// if neither button is on the bar, we need to install it
		if ((document.getElementById(smallButtonId) == null) &&
				(document.getElementById(largeButtonId) == null)) {

			var buttonAfterId = "button-tag";
			var buttonAfter = document.getElementById(buttonAfterId);
			if (buttonAfter != null) {
				var mailBar = buttonAfter.parentNode;
				// Options: WINNT, Darwin, Linux
				if (appInfo.OS == "Darwin") {
					// install large button by default
					mailBar.insertItem(largeButtonId, buttonAfter.nextSibling);
				} else {
					// install small button
					mailBar.insertItem(smallButtonId, buttonAfter.nextSibling);
				}

				mailBar.setAttribute("currentset", mailBar.currentSet );
				document.persist("mail-bar3", "currentset");
			}
		}
	}

	var ObserverService = Components.classes["@mozilla.org/observer-service;1"].getService(Components.interfaces.nsIObserverService);
	ObserverService.addObserver(CreateDbObserver, "MsgCreateDBView", false);

	/**
	 * This popup is shown when you right click on an email address contained
	 * in the body of an email
	 */
	var messagePaneContextPopup = document.getElementById("mailContext");
	messagePaneContextPopup.addEventListener("popupshowing", menuShowingEvent, false);
	messagePaneContextPopup.addEventListener("popuphiding", menuHidingEvent, false);

	/**
	 * This popup is shown when you click an email address directly above the
	 * email preview pane on the main Tbird window.
	 */
	var emailAddressPopup = document.getElementById("emailAddressPopup");
	emailAddressPopup.addEventListener("popupshowing", menuShowingEvent, false);
	emailAddressPopup.addEventListener("popuphiding", menuHidingEvent, false);

	/**
	 * This is the popup that is shown when you right click on folders in the
	 * left mail pane, ie Inbox, Sent, etc.
	 */
	var folderPaneContext = document.getElementById("folderPaneContext");
	folderPaneContext.addEventListener("popupshowing", onFolderPaneContextShowing, false);
	folderPaneContext.addEventListener("popuphiding", onFolderPaneContextHiding, false);

	doemailCheckForUpdate();
	window.setInterval(doemailCheckForUpdate, 60 * 60 * 1000); // check every hour

	/* Start the timer to update the pending email count */
	doemailUpdatePendingEmailCount();
	window.setInterval(doemailUpdatePendingEmailCount, 10 * 60 * 1000); // check every 10 min

	/** TODO: This was not enabled, leave alone.
	var observer = {
		observe: function (aSubject, aTopic, aState)
		{
			if (aTopic == "network:offline-status-changed")
				alert(aState);
		}
	}

	var os = Components.classes["@mozilla.org/observer-service;1"]
						.getService(Components.interfaces.nsIObserverService);
	os.addObserver(observer, "network:offline-status-changed", false);
	var ioService = Components.classes["@mozilla.org/network/io-service;1"]
									 .getService(Components.interfaces.nsIIOService);
	if (ioService.offline) {
		alert('offline');
	} else {
		alert('online');
	}
	**/

	/* Check if their doemail URL is set to https, if not set it
			Pull this after rev .13
	*/
	var prefManager = Components.classes["@mozilla.org/preferences-service;1"]
                                .getService(Components.interfaces.nsIPrefBranch);
	var doemailRoot = prefManager.getCharPref("extensions.doemail.doemailRoot");
	if (doemailRoot == "http://www.doemail.org/") {
		prefManager.setCharPref("extensions.doemail.doemailRoot", "https://www.doemail.org/");
	}

	/* Delete any leftover temporary files from viewing pending email that may still exist
		because we cannot with assuredness capture the cmd_quit event */
	// Get the temporary directory
	var dir = Components.classes["@mozilla.org/file/directory_service;1"]
                     .getService(Components.interfaces.nsIProperties)
                     .get("TmpD", Components.interfaces.nsIFile);
	var entries = dir.directoryEntries;
	var array = [];
	while(entries.hasMoreElements())
	{
		var entry = entries.getNext();
		entry.QueryInterface(Components.interfaces.nsIFile);
		if (entry.leafName.indexOf(pendingEmailTempFileName) != -1) {
			entry.remove(false);
		}
	}

	// Disable our tracking of deletions for research
	//window.setTimeout(initController, 1000);
}

function initController() {
	// Try to insert a handler to handle deletions ahead of Tbird!
	var defaultController = top.controllers.getControllerForCommand("cmd_delete");

	var DOEmailController =	{
		parentController: defaultController,
    supportsCommand: function(command)  {
			switch (command) {
				case "button_delete":
				case "cmd_delete":
				case "cmd_shiftDelete":
					return true;
				default:
					return false;
			}
		},
    isCommandEnabled: function(command)	{
			return this.parentController.isCommandEnabled(command);
		},
		doCommand: function(command) {
			switch (command) {
				case "button_delete":
				case "cmd_delete":
				case "cmd_shiftDelete":
					var indicesArray = {};
					var length = {};
					gDBView.getIndicesForSelection(indicesArray,length);
					var queryDone = false;
					var rows = new Array();
					var prefManager = Components.classes["@mozilla.org/preferences-service;1"]
				                                .getService(Components.interfaces.nsIPrefBranch);
					var lastDeleteQuery = prefManager.getIntPref("extensions.doemail.lastDeleteQuery");
					var now = new Date();
					var queryReady = false;

				// if last query was > 24 hours ago
					if ((lastDeleteQuery + (24*60*60)) < (now.getTime() / 1000)) {
						queryReady = true;
					}


					for (var i = 0; i < indicesArray.value.length; ++i) {
						if (indicesArray.value[i]) {
							var row = indicesArray.value[i];

							var key = gDBView.getKeyAt(row);
							var hdr = gDBView.db.GetMsgHdrForKey(key);
							var ruleType = hdr.getStringProperty("x-doemail");
							var spamScore = hdr.getStringProperty("x-doemail-score");
							//dump("Row:" + row + " key: " + key + " resultType: " + ruleType + "\n");

							// If we didn't pass through DOEmail, good bye
							if (!ruleType || (ruleType.length == 0)) {
								continue;
							}
							var queryResult = "";

							// Query the user if this sender/email was automated
							if (queryReady && !queryDone && (ruleType.length > 0) && (Math.ceil(Math.random() * 3) == 2)) {

								var promptService = Components.classes["@mozilla.org/embedcomp/prompt-service;1"]
																		.getService(Components.interfaces.nsIPromptService);

								var flags=promptService.BUTTON_TITLE_YES * promptService.BUTTON_POS_0 +
													promptService.BUTTON_TITLE_NO * promptService.BUTTON_POS_1 +
													promptService.BUTTON_DELAY_ENABLE;

								var d = new Date(hdr.dateInSeconds * 1000); // Date takes miliseconds

								var message = "Please assist our research by answering the following question:\n\n" +
															"Click \"Yes\" if the below email you are deleting is what you consider SPAM, otherwise click \"No\".\n\n" +
							                "Date: " + d.toLocaleString() + "\n" +
								              "Sender: " + hdr.mime2DecodedAuthor + "\n" +
							                "Subject: " + hdr.mime2DecodedSubject;
								var res = promptService.confirmEx(window, "Possible SPAM Research Query", message,
									flags, null, null, null, null, {});
								if (res == 0) {
									queryResult = "true";
								} else if (res == 1) {
									queryResult = "false";
								}

								queryDone = true;

								prefManager.setIntPref("extensions.doemail.lastDeleteQuery", now.getTime() / 1000);

								//dump("Query Result: " + queryResult + "\n");
							}

							rows.push({messageId: hdr.messageId,
								author: hdr.mime2DecodedAuthor,
								recipients: hdr.mime2DecodedRecipients,
								queryResult: queryResult,
								ruleType: ruleType,
								emailDateEpoch: hdr.dateInSeconds,
								spamScore: spamScore});
						}
					}
					queryEmailDelete(rows);

					// Call our parent controller to actually perform the command now
					this.parentController.doCommand(command);
					break;
				default:
					break
			}
		},
		onEvent: function(event) {
			switch (command) {
				case "button_delete":
				case "cmd_delete":
				case "cmd_shiftDelete":
					this.parentController.onEvent(event);
					break;
				default:
					break
			}
		}
	};

	// DISABLE THE DELETION TRACKER
	//top.controllers.insertControllerAt(0, DOEmailController);
}