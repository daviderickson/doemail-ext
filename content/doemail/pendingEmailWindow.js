function pendingEmailWindowInit() {
	var url = window.arguments[0];
	var browser = document.getElementById("pendingEmailWindowBrowser");
	browser.loadURI(url);
	browser.contentWindow.scrollbars.visible = true;
	window.focus();
}

function browserCopy() {
	const gClipboardHelper = Components.classes["@mozilla.org/widget/clipboardhelper;1"] .getService(Components.interfaces.nsIClipboardHelper);
	var browser = document.getElementById("pendingEmailWindowBrowser");
	var sel = browser.contentWindow.getSelection();
	var selString = sel.toString();
	gClipboardHelper.copyString(selString);
}