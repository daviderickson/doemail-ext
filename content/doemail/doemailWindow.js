window.addEventListener("load", initDOEmailWindow, false);

var globalTreeViews = new Object();
var gPendingEmailAndSubjectsTimeout = 300000;
var gSearchInput = null;
var gIgnoreFocus = false;
var gIgnoreClick = false;
// temporary global used to make sure we restore focus to the correct element after clearing the quick search box
// because clearing quick search means stealing focus.
var gQuickSearchFocusEl = null;
var gSearchTimer = null;

function initDOEmailWindow() {
	window.title = window.title + " " + doemail_version;
	// XXX TODO in thunderbird 3 hopefully persist will actually work!
	var tabbox = document.getElementById("doemailWindowPrimaryTabbox");
	tabbox.selectedIndex = 6
}

function getSearchInput() {
	if (!gSearchInput) {
		gSearchInput = document.getElementById("doemailSearchInput");
	}
	return gSearchInput;
}

function onSearchInputFocus(event) {
  getSearchInput();

  // search bar has focus, ...clear the showing search criteria flag
  if (gSearchInput.showingSearchCriteria) {
    gSearchInput.value = "";
    gSearchInput.showingSearchCriteria = false;
  }

  if (gIgnoreFocus) { // got focus via mouse click, don't need to anything else
    gIgnoreFocus = false;
  } else {
    gSearchInput.select();
    gQuickSearchFocusEl = gSearchInput;   // only important that this be non-null
  }
}

function onSearchInputMousedown(event) {
  getSearchInput();
  if (gSearchInput.hasAttribute("focused")) {
    gIgnoreClick = true;

    // already focused, don't need to restore focus elsewhere (if the Clear button was clicked)
    // ##HACK## Need to check 'clearButtonHidden' because the field is blurred when it has
    // focus and the hidden button is clicked, in which case we want to perform the normal
    // onBlur function.
    gQuickSearchFocusEl = gSearchInput.clearButtonHidden ? gSearchInput : null;
  } else {
    gIgnoreFocus = true;
    gIgnoreClick = false;

    // save the last focused element so that focus can be restored (if the Clear button was clicked)
    gQuickSearchFocusEl = null;
  }
  // (if Clear button was clicked, onClearSearch() is called before onSearchInputClick())
}


function onSearchInputClick(event) {
  if (!gIgnoreClick) {
    gQuickSearchFocusEl = null; // ##HACK## avoid onSearchInputBlur() side effects
    gSearchInput.select();      // ## triggers onSearchInputBlur(), but focus returns to field
  }
  if (!gQuickSearchFocusEl)
    gQuickSearchFocusEl = gSearchInput;   // mousedown wasn't on Clear button
}

function onSearchInputBlur(event) {
  //if (!gQuickSearchFocusEl) // ignore the blur if we are in the middle of processing the clear button
  //  return;

  gQuickSearchFocusEl = null;
  if (!gSearchInput.value)
    gSearchInput.showingSearchCriteria = true;

  if (gSearchInput.showingSearchCriteria)
    gSearchInput.setSearchCriteriaText();
}

function onSearchInput() {
}

function onSearchKeyPress(event) {
	if (gSearchInput.showingSearchCriteria)
		gSearchInput.showingSearchCriteria = false;

	// 13 == return
	if (event && event.keyCode == 13) {
		var search = getSearchInput().value;
		dump("Search Value: " + search + "\n");
		if (search.length == 0) {
			onClearSearch();
			document.getElementById("pendingEmail").focus();
			return;
		}
		var encodedData="search=" + encodeURIComponent(json_encode(new Array(search)));
		doemailRequest("searchPendingEmail", encodedData, doemailLoadCallback, {	treeId: "pendingEmail",
																																							 treeView: pendingEmailTreeView,
																																							email: "sender",
																																							dataPostProcessing: pendingEmailDataInit,
																																							countLabelId: "pendingEmailCountLabel",
																																							timeout: gPendingEmailAndSubjectsTimeout});
		gSearchInput.select();
	}
}

function onClearSearch() {
	gSearchInput.showingSearchCriteria = true;
	gSearchInput.clearButtonHidden = true;
	gSearchInput.setSearchCriteriaText();
	gIgnoreClick = true;
	doemailRequest("getPendingEmailAndSubjects", "", doemailLoadCallback, {	treeId: "pendingEmail",
																																					 	treeView: pendingEmailTreeView,
																																						email: "sender",
																																						dataPostProcessing: pendingEmailDataInit,
																																						refresh: true,
																																						countLabelId: "pendingEmailCountLabel",
																																						timeout: gPendingEmailAndSubjectsTimeout});
}

function getSpamScoreFilterValue() {
	var menu = document.getElementById("spamScoreFilterMenu");
	return menu.value;
}

function spamScoreFilterValueUpdate(value, treeId) {
	var tree = globalTreeViews[treeId];
	tree.setSpamScoreFilterValue(value);
	tree.filterData();
	tree.sortData();
	document.getElementById(treeId).treeBoxObject.invalidate();
}

function addTextboxKeyPress(event, element) {
	if (KeyEvent.DOM_VK_RETURN == event.keyCode) {
		doemailAdd(element.id);
	}
}

function openPendingEmailWindowContext(node) {
	var treeId = node.parentNode.id;
	openPendingEmailWindow(treeId);
}

function openPendingEmailWindowRawContext(node) {
	var treeId = node.parentNode.id;
	openPendingEmailWindowRaw(treeId);
}

function openPendingEmailWindow(treeId) {
	/* find first selected email, we'll open it */
	var tree = document.getElementById(treeId);
	var data = globalTreeViews[treeId].data;
	var selection = tree.view.selection;
	var sender = "";
	var file = "";

	for (var i = 0; i < data.length; ++i) {
		if (selection.isSelected(i)) {
			sender = data[i].sender;
			file = data[i].file;
			break;
		}
	}

	if (file.length > 0) {
		var prefManager = Components.classes["@mozilla.org/preferences-service;1"]
	                                .getService(Components.interfaces.nsIPrefBranch);
		var doemailUsername = prefManager.getCharPref("extensions.doemail.doemailUsername");
		var doemailRoot = prefManager.getCharPref("extensions.doemail.doemailRoot");
		var doemailPassword = prefManager.getCharPref("extensions.doemail.doemailPassword");
		doemailPassword = SHA256(doemailPassword);
		var doemailDebug = prefManager.getBoolPref("extensions.doemail.doemailDebug");

		var url = doemailRoot + "viewPendingExtension.php?";

		var data = "username=" + encodeURIComponent(doemailUsername) + "&password=" +
			encodeURIComponent(doemailPassword) + "&s=" + encodeURIComponent(sender) +
			"&f=" + encodeURIComponent(file);

		url = url + data;

		// create the temporary file to download our email into
		var tempFile = Components.classes["@mozilla.org/file/directory_service;1"]
												 .getService(Components.interfaces.nsIProperties)
												 .get("TmpD", Components.interfaces.nsIFile);
		tempFile.append(pendingEmailTempFileName);
		tempFile.createUnique(Components.interfaces.nsIFile.NORMAL_FILE_TYPE, 0600);

		// create the URI for the email on the webserver
		var ios = Components.classes["@mozilla.org/network/io-service;1"]
			.getService(Components.interfaces.nsIIOService);
		var uri = ios.newURI(url, null, null);

		// create the URI for the tempFile it will be saved to
		var tempFileURI = ios.newURI("file://" + tempFile.path, null, null);
		if (tempFileURI instanceof Components.interfaces.nsIURL) {
			tempFileURI.query = "type=application/x-message-display";
		}

		// Progress listener
		var progressListener = {
			mnsIWebProgressListener: Components.interfaces.nsIWebProgressListener,

			QueryInterface: function(aIID) {
			 if (aIID.equals(this.mnsIWebProgressListener) ||
					 aIID.equals(Components.interfaces.nsISupports))
				 return this;
			 throw Components.results.NS_NOINTERFACE;
			},

			destroy: function() {
			},

			onStateChange: function(aProgress, aRequest, aFlag, aStatus) {
				// once it's done loading...
				if ((aFlag & this.mnsIWebProgressListener.STATE_STOP) && aStatus == 0) {
					try {
						newWin = window.openDialog( "chrome://messenger/content/messageWindow.xul", "_blank", "all,chrome,dialog=no,status,toolbar", tempFileURI, null, null );
					  newWin.addEventListener("close",
								function (event){
									tempFile.remove(false);
									return true;
								}
								,false);
						/*
						newWin.addEventListener("load",
								function (event) {
									newWin.addEventListener("close", function (event) { alert("close"); }, false);
									newWin.addEventListener("unload", function (event) { alert("unload"); }, false);
								}, false);
						*/
					} catch (ex) {
						dump("Failure loading external file");
					} finally {
						this.destroy();
					}
				}
				return 0;
			},

			onLocationChange: function() {return 0;},
			onProgressChange: function() {return 0;},
			onStatusChange: function() {return 0;},
			onSecurityChange: function() {return 0;},
			onLinkIconAvailable: function() {return 0;}
		}

		var webBrowserPersist = Components
			.classes["@mozilla.org/embedding/browser/nsWebBrowserPersist;1"]
			.createInstance(Components.interfaces.nsIWebBrowserPersist);
		webBrowserPersist.progressListener = progressListener;
		webBrowserPersist.saveURI(uri, null, null, null, null, tempFile, null);
	}
}

function openPendingEmailWindowRaw(treeId) {
	/* find first selected email, we'll open it */
	var tree = document.getElementById(treeId);
	var data = globalTreeViews[treeId].data;
	var selection = tree.view.selection;
	var sender = "";
	var file = "";

	for (var i = 0; i < data.length; ++i) {
		if (selection.isSelected(i)) {
			sender = data[i].sender;
			file = data[i].file;
			break;
		}
	}

	if (file.length > 0) {
		var prefManager = Components.classes["@mozilla.org/preferences-service;1"]
	                                .getService(Components.interfaces.nsIPrefBranch);
		var doemailRoot = prefManager.getCharPref("extensions.doemail.doemailRoot");
		var doemailUsername = prefManager.getCharPref("extensions.doemail.doemailUsername");
		var doemailPassword = prefManager.getCharPref("extensions.doemail.doemailPassword");
		doemailPassword = SHA256(doemailPassword);
		var doemailDebug = prefManager.getBoolPref("extensions.doemail.doemailDebug");

		var url = doemailRoot + "viewPendingExtension.php?";

		var data = "username=" + encodeURIComponent(doemailUsername) + "&password=" +
			encodeURIComponent(doemailPassword) + "&s=" + encodeURIComponent(sender) +
			"&f=" + encodeURIComponent(file);

		url = url + data;

		window.openDialog("chrome://doemail/content/pendingEmailWindow.xul","pendingEmailWindow","chrome,resizable=yes,dialog=no,scrollbars=yes", url);
	}
}

function doemailAdd(textboxId) {
	var strbundle = document.getElementById("doemailStringBundle");
	var value = document.getElementById(textboxId).value;
	if (!value || (value.length == 0)) {
		return;
	}
	var matched = true;

	switch (textboxId) {
		case "doemailAddWhitelistEmailAddress":
			if (emailRegex.exec(value) == null) {
				alert(value + " " + strbundle.getString("doemail.mainDialog.emailAddress.invalid"));
				return;
			}
			var boxobject = document.getElementById("whitelistEmail").boxObject;
			boxobject.QueryInterface(Components.interfaces.nsITreeBoxObject);
			var scrollIndex = boxobject.getFirstVisibleRow();
			addWhitelistEmail([value], doemailDataRefreshRequestCallback, {request: "getWhitelistEmail", treeId: "whitelistEmail", email: "email", refresh: true, scrollIndex: scrollIndex, countLabelId: "whitelistEmailCountLabel"});
			break;
		case "doemailAddWhitelistDomain":
			if (domainRegex.exec(value) == null) {
				alert(value + " " + strbundle.getString("doemail.mainDialog.domain.invalid"));
				return;
			}
			var boxobject = document.getElementById("whitelistDomain").boxObject;
			boxobject.QueryInterface(Components.interfaces.nsITreeBoxObject);
			var scrollIndex = boxobject.getFirstVisibleRow();
			addWhitelistDomain([value], doemailDataRefreshRequestCallback, {request: "getWhitelistDomain", treeId: "whitelistDomain", refresh: true, scrollIndex: scrollIndex, countLabelId: "whitelistDomainCountLabel"});
			break;
		case "doemailAddBlacklistEmailAddress":
			if (emailRegex.exec(value) == null) {
				alert(value + " " + strbundle.getString("doemail.mainDialog.emailAddress.invalid"));
				return;
			}
			var boxobject = document.getElementById("blacklistEmail").boxObject;
			boxobject.QueryInterface(Components.interfaces.nsITreeBoxObject);
			var scrollIndex = boxobject.getFirstVisibleRow();
			addBlacklistEmail([value], doemailDataRefreshRequestCallback, {request: "getBlacklistEmail", treeId: "blacklistEmail", email: "email", refresh: true, scrollIndex: scrollIndex, countLabelId: "blacklistEmailCountLabel"});
			break;
		case "doemailAddBlacklistDomain":
			if (domainRegex.exec(value) == null) {
				alert(value + " " + strbundle.getString("doemail.mainDialog.domain.invalid"));
				return;
			}
			var boxobject = document.getElementById("blacklistDomain").boxObject;
			boxobject.QueryInterface(Components.interfaces.nsITreeBoxObject);
			var scrollIndex = boxobject.getFirstVisibleRow();
			addBlacklistDomain([value], doemailDataRefreshRequestCallback, {request: "getBlacklistDomain", treeId: "blacklistDomain", refresh: true, scrollIndex: scrollIndex, countLabelId: "blacklistDomainCountLabel"});
			break;
		case "doemailAddMailingListWhitelistEmailAddress":
			if (emailRegex.exec(value) == null) {
				alert(value + " " + strbundle.getString("doemail.mainDialog.emailAddress.invalid"));
				return;
			}
			var boxobject = document.getElementById("whitelistTo").boxObject;
			boxobject.QueryInterface(Components.interfaces.nsITreeBoxObject);
			var scrollIndex = boxobject.getFirstVisibleRow();
			addWhitelistTo([value], doemailDataRefreshRequestCallback, {request: "getWhitelistTo", treeId: "whitelistTo", email: "tovalue", refresh: true, scrollIndex: scrollIndex, countLabelId: "whitelistToCountLabel"});
			break;
		case "doemailAddVirtualEmailAddressExtension":
			if (value.indexOf('@') != -1) {
				alert(value + " " + strbundle.getString("doemail.mainDialog.virtual.invalid"));
				return;
			}
			var boxobject = document.getElementById("codesTree").boxObject;
			boxobject.QueryInterface(Components.interfaces.nsITreeBoxObject);
			var scrollIndex = boxobject.getFirstVisibleRow();
			var description = document.getElementById("doemailDescription").value;
			var columnDecorator = new Object();
			columnDecorator.creation_date = dateRender;
			var data = new Object();
			data.code = value;
			data.description = description;
			addCode([data], doemailDataRefreshRequestCallback, {request: "getCodes",
																																			treeId: "codesTree",
																																			dataPostProcessing: codesTreeDataInit,
																																			countLabelId: "codesCountLabel",
																																			columnDecorator: columnDecorator,
																																			refresh: true,
																																			scrollIndex: scrollIndex});
			break;
		default:
			matched = false;
			break;
	}

	if (matched) {
		document.getElementById(textboxId).value = "";
		document.getElementById("doemailDescription").value = "";
	}
	document.getElementById(textboxId).focus();
}

function keyCtlA(node) {
	var treeId = node.id;
	var tree = globalTreeViews[treeId];
	if (tree != null) {
		var data = tree.data;
		var selection = node.view.selection;

		// modify selection to select all
		selection.selectAll();
		document.getElementById(treeId).treeBoxObject.invalidate();
	}
}

function keyCtlC(node) {
	var treeId = node.id;
	doemailDialogCopy(treeId);
}

function doemailDialogContextMenuCopy(node) {
	var treeId = node.parentNode.id;
	doemailDialogCopy(treeId);
}

function doemailDialogCopy(treeId) {
	var tree = document.getElementById(treeId);
	var data = globalTreeViews[treeId].data;
	var selection = tree.view.selection;
	var result = "";

	for (var i = 0; i < data.length; ++i) {
		if (selection.isSelected(i)) {
			if (result.length > 0) {
				result += " ";
			}

			switch (treeId) {
				case "whitelistEmail":
					result += data[i].email;
					break;
				case "whitelistDomain":
					result += data[i].domain;
					break;
				case "blacklistEmail":
					result += data[i].email;
					break;
				case "blacklistDomain":
					result += data[i].domain;
					break;
				case "whitelistTo":
					result += data[i].tovalue;
					break;
				case "codesTree":
					result += data[i].email;
					break;
			}
		}
	}

	const gClipboardHelper = Components.classes["@mozilla.org/widget/clipboardhelper;1"] .getService(Components.interfaces.nsIClipboardHelper);
	gClipboardHelper.copyString(result);
}

function delKeyRemove(node) {
	var treeId = node.id;
	var tree = globalTreeViews[treeId];
	if (tree != null) {
		var data = tree.data;
		//var index = node.view.selection.currentIndex;
		var selection = node.view.selection;

		if ("pendingEmail" == treeId) {
			pendingEmailRemove("pendingEmail", "none");	
		} else {
			remove(treeId, data, selection);
		}
	}
}

function contextMenuRemove(node) {
	var treeId = node.parentNode.id;
	var data = globalTreeViews[treeId].data;
	//var index = node.parentNode.view.selection.currentIndex;
	var selection = node.parentNode.view.selection;

	remove(treeId, data, selection);
}

function remove(treeId, data, selection) {
	var values = new Array();

	// Get Scroll Information
	var tree = document.getElementById(treeId);
	var boxobject = tree.boxObject;
	boxobject.QueryInterface(Components.interfaces.nsITreeBoxObject);
	var scrollIndex = boxobject.getFirstVisibleRow();

	if ((treeId.indexOf("Email") != -1) && (treeId != "pendingEmail")) {
		// Push all selected entries into the array
		for (var i = 0; i < data.length; ++i) {
			if (selection.isSelected(i)) {
				values.push(data[i].email);
			}
		}

		if (treeId.indexOf("white") != -1) {
			removeWhitelistEmail(values, doemailDataRefreshRequestCallback, {request: "getWhitelistEmail", treeId: "whitelistEmail", email: "email", refresh: true, scrollIndex: scrollIndex, countLabelId: "whitelistEmailCountLabel"});
		} else {
			removeBlacklistEmail(values, doemailDataRefreshRequestCallback, {request: "getBlacklistEmail", treeId: "blacklistEmail", email: "email", refresh: true, scrollIndex: scrollIndex, countLabelId: "blacklistEmailCountLabel"});
		}
	} else if (treeId.indexOf("Domain") != -1) {
		// Push all selected entries into the array
		for (var i = 0; i < data.length; ++i) {
			if (selection.isSelected(i)) {
				values.push(data[i].domain);
			}
		}

		if (treeId.indexOf("white") != -1) {
			removeWhitelistDomain(values, doemailDataRefreshRequestCallback, {request: "getWhitelistDomain", treeId: "whitelistDomain", refresh: true, scrollIndex: scrollIndex, countLabelId: "whitelistDomainCountLabel"});
		} else {
			removeBlacklistDomain(values, doemailDataRefreshRequestCallback, {request: "getBlacklistDomain", treeId: "blacklistDomain", refresh: true, scrollIndex: scrollIndex, countLabelId: "blacklistDomainCountLabel"});
		}
	} else if (treeId == "whitelistTo") {
		// whitelistTo
		// Push all selected entries into the array
		for (var i = 0; i < data.length; ++i) {
			if (selection.isSelected(i)) {
				values.push(data[i].tovalue);
			}
		}

		removeWhitelistTo(values, doemailDataRefreshRequestCallback, {request: "getWhitelistTo", treeId: "whitelistTo", email: "tovalue", refresh: true, scrollIndex: scrollIndex, countLabelId: "whitelistToCountLabel"});
	} else if (treeId == "codesTree") {
		// Push all selected entries into the array
		for (var i = 0; i < data.length; ++i) {
			if (selection.isSelected(i)) {
				values.push(data[i].code);
			}
		}

		var columnDecorator = new Object();
		columnDecorator.creation_date = dateRender;

		removeCode(values, doemailDataRefreshRequestCallback, {request: "getCodes", treeId: "codesTree",
																													dataPostProcessing: codesTreeDataInit,
																													columnDecorator: columnDecorator,
																													countLabelId: "codesCountLabel",
																													refresh: true,
																													scrollIndex: scrollIndex});
	}
}

function contextApprove(node, type) {
	var treeId = node.parentNode.id;
	pendingApprove(treeId, type);
}

function pendingApprove(treeId, type) {
	var tree = document.getElementById(treeId);
	var data = globalTreeViews[treeId].data;
	var selection = tree.view.selection;
	//var index = node.parentNode.view.selection.currentIndex;
	var result = new Object();
	var boxobject = tree.boxObject;
	boxobject.QueryInterface(Components.interfaces.nsITreeBoxObject);
	var scrollIndex = boxobject.getFirstVisibleRow();

	// build a transaction id
	var now = new Date();
	var transaction = doemailGetUsername() + now.getTime();

	var senderAndFilePairs = new Array();
	var queryDone = false;

	for (var i = 0; i < data.length; ++i) {
		if (selection.isSelected(i)) {
			// walk up the data structure until we find the parent container
			var index = i;
			while ((index >= 0) && (data[index].level != 0)) {
				--index;
			}

			var query = false;
			var queryResult = "";
			// Query the user if this sender/email was automated
			if (!queryDone && ("domain" != type) && (Math.ceil(Math.random() * 3) == 2) && false) { // DISABLED

				var promptService = Components.classes["@mozilla.org/embedcomp/prompt-service;1"]
														.getService(Components.interfaces.nsIPromptService);

				var flags=promptService.BUTTON_TITLE_IS_STRING * promptService.BUTTON_POS_0 +
									promptService.BUTTON_TITLE_IS_STRING * promptService.BUTTON_POS_1 +
									promptService.BUTTON_DELAY_ENABLE;

				dump(data[index].date + "\n");
				var d = new Date(data[index].date * 1000);

				var message = "Please assist our research by answering the following question:\n\n" +
											"Click \"Automated\" if the below email and/or sender appears to be automated (eg. newsletter, receipt, confirmation, etc.), or click \"Human\" if you believe it to have been sent by a legitimate human being.\n\n" +
			                "Date: " + d.toLocaleString() + "\n" +
				              "Sender: " + data[index].sender + "\n" +
			                "Subject: " + data[index].subject;
				var res = promptService.confirmEx(window, "Pending Email Research Query", message,
					flags, "Automated", "Human", null, null, {});
				if (res == 0) {
					queryResult = "true";
				} else if (res == 1) {
					queryResult = "false";
				}
				query = true;
				queryDone = true;
				dump("Query Result: " + queryResult + "\n");
			}

			if ("email" == type) {
				if (!result[data[index].sender]) {
					var obj = new Object();
					obj.email = data[index].sender;
					if (query) {
						obj.query = queryResult;
					}
					result[data[index].sender] = obj;
				} else {
					// already exists, did we query? if so update
					if (query) {
						result[data[index].sender].query = queryResult;
					}
				}
			} else if ("domain" == type) {
				var domain = data[index].sender.substring(data[index].sender.indexOf("@")+1, data[index].sender.length);
				if (!result[domain]) {
					result[domain] = true;
				}
			} else if ("none" == type){
				var obj = new Object();
				obj.email = data[index].sender;
				obj.file = data[index].file;
				if (query) {
					obj.query = queryResult;
				}
				senderAndFilePairs.push(obj);
			}
		}
	}

	// whitelist and approve email
	var approveCallbackOptions = {request: "getPendingEmailAndSubjects",
																treeId: "pendingEmail",
																treeView: pendingEmailTreeView,
																email: "sender",
																dataPostProcessing: pendingEmailDataInit,
																refresh: true,
																scrollIndex: scrollIndex,
																countLabelId: "pendingEmailCountLabel",
																transaction: transaction};

	if ("email" == type) {
		var senders = new Array();
		for(var prop in result) {
			senders.push(result[prop]);
		}
		// rip our senders from the unique object
		addWhitelistEmail(senders, doemailDataRefreshRequestCallback, {request: "getWhitelistEmail",
																																	treeId: "whitelistEmail",
																																	email: "email",
																																	refresh: true,
																																	stopProgressMeter: false,
																																	countLabelId: "whitelistEmailCountLabel",
																																	transaction: transaction});
		approveEmailSenders(senders, doemailDataRefreshRequestCallback, approveCallbackOptions);
	} else if ("domain" == type) {
		var domains = new Array();
		for(var prop in result) {
			domains.push(prop);
		}
		addWhitelistDomain(domains, doemailDataRefreshRequestCallback, {request: "getWhitelistDomain",
																																	treeId: "whitelistDomain",
																																	refresh: true,
																																	stopProgressMeter: false,
																																	countLabelId: "whitelistDomainCountLabel",
																																	transaction: transaction});
		approveEmailDomains(domains, doemailDataRefreshRequestCallback, approveCallbackOptions);
	} else if ("none" == type) {
		approveSpecificEmail(senderAndFilePairs, doemailDataRefreshRequestCallback, approveCallbackOptions);
	}
}

function pendingEmailContextRemove(node, type) {
	var treeId = node.parentNode.id;
	pendingEmailRemove(treeId, type);
}

function pendingEmailRemove(treeId, type) {
	var data = globalTreeViews[treeId].data;
	var tree = document.getElementById(treeId);
	var selection = tree.view.selection;

	var values = new Array();

	var boxobject = tree.boxObject;
	boxobject.QueryInterface(Components.interfaces.nsITreeBoxObject);
	var scrollIndex = boxobject.getFirstVisibleRow();

	for (var i = 0; i < data.length; ++i) {
		if (selection.isSelected(i)) {
			// walk up the data structure until we find the parent container
			var index = i;
			while ((index >= 0) && (data[index].level != 0)) {
				--index;
			}
			if (type == "none") {
			  var obj = new Object();
				obj.sender = data[index].sender;
				obj.file = data[index].file;
				values.push(obj);
			} else {
				if (values.indexOf(data[index].sender) == -1) {
					values.push(data[index].sender);
				}
			}
		}
	}

	var callbackOptions = {request: "getPendingEmailAndSubjects",
													treeId: "pendingEmail",
													treeView: pendingEmailTreeView,
													email: "sender",
													dataPostProcessing: pendingEmailDataInit,
													refresh: true,
	                        scrollIndex: scrollIndex,
													countLabelId: "pendingEmailCountLabel"};

	if (type == "none") {
		removeSpecificEmail(values, doemailDataRefreshRequestCallback, callbackOptions);

	} else if (type == "email") {
		addBlacklistEmail(values, doemailDataRefreshRequestCallback, {request: "getBlacklistEmail",
																																	treeId: "blacklistEmail",
																																	email: "email",
																																	refresh: true,
																																	countLabelId: "blacklistEmailCountLabel"});

		removeEmailBySenders(values, doemailDataRefreshRequestCallback, callbackOptions);
	} else if (type == "domain") {
		var domains = new Array();
		for (index in values) {
			domains.push(values[index].substring(values[index].indexOf("@")+1, values[index].length));
		}

		addBlacklistDomain(domains, doemailDataRefreshRequestCallback, {request: "getBlacklistDomain",
																																	treeId: "blacklistDomain",
																																	refresh: true,
																																	countLabelId: "blacklistDomainCountLabel"});
		removeEmailByDomains(domains, doemailDataRefreshRequestCallback, callbackOptions);
	}
}

function pendingSubmit() {
	// find out which radio box was selected
	var pendingRadioGroup = document.getElementById("pendingRadioGroup");
	if (pendingRadioGroup.selectedIndex == -1) {
		return;
	}

	var index = pendingRadioGroup.selectedIndex;
	switch (index) {
		case 0:
			pendingApprove("pendingEmail", "none")
			break;
		case 1:
			pendingApprove("pendingEmail", "email")
			break;
		case 2:
			pendingApprove("pendingEmail", "domain")
			break;
		case 3:
			pendingEmailRemove("pendingEmail", "none");
			break;
		case 4:
			pendingEmailRemove("pendingEmail", "email");
			break;
		case 5:
			pendingEmailRemove("pendingEmail", "domain");
			break;
	}
}

function generateCode(textboxId) {
	var chars = "abcdefghijklmnopqrstuvwxyz0123456789";
	var code = "";
	for (var i = 0; i < 8; ++i) {
		code = code + chars.charAt(Math.round(Math.random() * chars.length));
	}

	var textbox = document.getElementById(textboxId);
	textbox.value = code;
}

function doemailDataRefreshRequestCallback(xmlResponse, options) {
	doemailRequest(options.request, "", doemailLoadCallback, options);
}

function doemailLoadCallback(data, options) {
	var tree = document.getElementById(options.treeId);

	if ((data.exitCode != null) && (data.exitCode == 1)) {
		setDoemailStatusText("Error: " + data.message);
		return;
	}

	//var data = parseXMLResponse(xmlResponse, options);
	if (options.dataPostProcessing) {
		options.dataPostProcessing(data);
	}

	if (options.countLabelId) {
		var strbundle = document.getElementById("doemailStringBundle");
		var countString = strbundle.getString("doemail.mainDialog.count.label");
		var label = document.getElementById(options.countLabelId);
		label.value = countString + " " + data.length;
	}

	var treeView = null;
	if (options.treeView) {
		treeView = options.treeView();
	} else {
		treeView = newTreeView();
	}

	treeView.setRowCount(data.length);
	treeView.setData(data);
	if (options.columnDecorator) {
		treeView.columnDecorator = options.columnDecorator;
	}

	// If this is a load for the pending email, filter if necessary
	if (treeView.hasOwnProperty("spamScoreFilterValue")) {
		treeView.setSpamScoreFilterValue(getSpamScoreFilterValue());
		treeView.filterData();
	}

	if (options.refresh && (options.refresh == true)) {
		var oldTreeView = globalTreeViews[options.treeId];
		if (oldTreeView.sortedColumn) {
			treeView.sortedColumn = oldTreeView.sortedColumn;
			treeView.sortedDirection = oldTreeView.sortedDirection;
			treeView.sortData();
		}
	} else {
		// fresh load of this tree, check for a sorted column
		var sortedCol = tree.columns.getSortedColumn();
		if (sortedCol != null) {
			treeView.sortedColumn = sortedCol;
			if ((sortedCol.element.getAttribute("sortedColumn") != null) &&
			    ("descending" == sortedCol.element.getAttribute("sortDirection"))) {
				treeView.sortedDirection = 1;
			} else {
				treeView.sortedDirection = 0;
			}
			treeView.sortData();
		}
	}

	tree.view = treeView;
	globalTreeViews[options.treeId] = treeView;

	// Set Scroll Data
	if (options.scrollIndex) {
		var boxobject = tree.boxObject;
		boxobject.QueryInterface(Components.interfaces.nsITreeBoxObject);

		if (treeView.rowCount == 0) {
			// no rows left
			boxobject.scrollToRow(0);
		} else if ((options.scrollIndex + boxobject.getPageLength()) < treeView.rowCount) {
			boxobject.scrollToRow(options.scrollIndex);
		} else {
			// scroll index past the total rows left, show the last page
			boxobject.ensureRowIsVisible(treeView.rowCount-1);
		}
	}

	tree.treeBoxObject.invalidate();
}

function dateRender(row, column, data) {
	var seconds = Number(data[row][column.id]);
	var milliseconds = seconds * 1000;
	var d = new Date(milliseconds);
	return (d.getMonth()+1) + "-" + d.getDate() + "-" + d.getFullYear();
}

/**
    Called when Window is initialized
**/
function doemailWindowLoad() {
	//document.getElementById("doemailMenuList").addEventListener("ValueChange", doemailMenuListChanged, false)

	doemailRequest("getWhitelistEmail", "", doemailLoadCallback, {treeId: "whitelistEmail", email: "email", countLabelId: "whitelistEmailCountLabel", stopProgressMeter: false});
	doemailRequest("getWhitelistDomain", "", doemailLoadCallback, {treeId: "whitelistDomain", countLabelId: "whitelistDomainCountLabel", stopProgressMeter: false});
	doemailRequest("getBlacklistEmail", "", doemailLoadCallback, {treeId: "blacklistEmail", email: "email", countLabelId: "blacklistEmailCountLabel", stopProgressMeter: false});
	doemailRequest("getBlacklistDomain", "", doemailLoadCallback, {treeId: "blacklistDomain", countLabelId: "blacklistDomainCountLabel", stopProgressMeter: false});
	doemailRequest("getWhitelistTo", "", doemailLoadCallback, {treeId: "whitelistTo", email: "tovalue", countLabelId: "whitelistToCountLabel", stopProgressMeter: false});

	var columnDecorator = new Object();
	columnDecorator.creation_date = dateRender;
	doemailRequest("getCodes", "", doemailLoadCallback, {	treeId: "codesTree",
																															dataPostProcessing: codesTreeDataInit,
																															countLabelId: "codesCountLabel",
																															columnDecorator: columnDecorator,
																															stopProgressMeter: false});
	doemailRequest("getPendingEmailAndSubjects", "", doemailLoadCallback, {	treeId: "pendingEmail",
																																					 	treeView: pendingEmailTreeView,
																																						email: "sender",
																																						dataPostProcessing: pendingEmailDataInit,
																																						countLabelId: "pendingEmailCountLabel",
																																						timeout: gPendingEmailAndSubjectsTimeout});

	/* Hook our context menus */
	var pendingEmailContextPopup = document.getElementById("pendingEmailContext");
	pendingEmailContextPopup.addEventListener("popupshowing", pendingEmailContextPopupShowing, false);
	pendingEmailContextPopup.addEventListener("popuphiding", pendingEmailContextPopupHiding, false);
}

function pendingEmailContextPopupShowing() {
	var viewEmailMenuItem = document.getElementById("viewEmailMenuItem");
	var viewRawEmailMenuItem = document.getElementById("viewRawEmailMenuItem");
	var tree = document.getElementById("pendingEmail");
	var selection = tree.view.selection;
	if (selection.count == 1) {
		viewEmailMenuItem.setAttribute("disabled", false);
		viewRawEmailMenuItem.setAttribute("disabled", false);
	}
}

function pendingEmailContextPopupHiding() {
	var viewEmailMenuItem = document.getElementById("viewEmailMenuItem");
	var viewRawEmailMenuItem = document.getElementById("viewRawEmailMenuItem");
	viewEmailMenuItem.setAttribute("disabled", true);
	viewRawEmailMenuItem.setAttribute("disabled", true);
}

function doemailRefreshAllData() {
	doemailRequest("getWhitelistEmail", "", doemailLoadCallback, {treeId: "whitelistEmail", email: "email", refresh: true, countLabelId: "whitelistEmailCountLabel", stopProgressMeter: false});
	doemailRequest("getWhitelistDomain", "", doemailLoadCallback, {treeId: "whitelistDomain", refresh: true, countLabelId: "whitelistDomainCountLabel", stopProgressMeter: false});
	doemailRequest("getBlacklistEmail", "", doemailLoadCallback, {treeId: "blacklistEmail", email: "email", refresh: true, countLabelId: "blacklistEmailCountLabel", stopProgressMeter: false});
	doemailRequest("getBlacklistDomain", "", doemailLoadCallback, {treeId: "blacklistDomain", refresh: true, countLabelId: "blacklistDomainCountLabel", stopProgressMeter: false});
	doemailRequest("getWhitelistTo", "", doemailLoadCallback, {treeId: "whitelistTo", email: "tovalue", refresh: true, countLabelId: "whitelistToCountLabel", stopProgressMeter: false});
	var columnDecorator = new Object();
	columnDecorator.creation_date = dateRender;
	doemailRequest("getCodes", "", doemailLoadCallback, {	treeId: "codesTree",
																															dataPostProcessing: codesTreeDataInit,
																															columnDecorator: columnDecorator,
																															countLabelId: "codesCountLabel",
																															refresh: true,
																															stopProgressMeter: false});
	doemailRequest("getPendingEmailAndSubjects", "", doemailLoadCallback, {	treeId: "pendingEmail",
																																					 	treeView: pendingEmailTreeView,
																																						email: "sender",
																																						dataPostProcessing: pendingEmailDataInit,
																																						refresh: true,
																																						countLabelId: "pendingEmailCountLabel",
																																						timeout: gPendingEmailAndSubjectsTimeout});
}

function doemailMenuListChanged(event) {
	var menulist = document.getElementById("doemailMenuList");
	var descriptionTextBox = document.getElementById("doemailDescription");
	var descriptionLabel = document.getElementById("doemailDescriptionLabel");
	var generateCodeButton = document.getElementById("doemailGenerateCodeButton");
	if ("codes" == menulist.selectedItem.value) {
		descriptionTextBox.disabled = false;
		generateCodeButton.disabled = false;
		descriptionLabel.disabled = false;
	} else {
		descriptionTextBox.disabled = true;
		descriptionTextBox.value = "";
		descriptionLabel.disabled = true;
		generateCodeButton.disabled = true;
	}
}

function codesTreeDataInit(data) {
	var prefManager = Components.classes["@mozilla.org/preferences-service;1"]
															.getService(Components.interfaces.nsIPrefBranch);
	var emailAddress = prefManager.getCharPref("extensions.doemail.doemailUsername");
	var delimiter = prefManager.getCharPref("extensions.doemail.doemailDelimiter");
	var local = emailAddress.substring(0, emailAddress.indexOf("@"));
	var domain = emailAddress.substring(emailAddress.indexOf("@")+1, emailAddress.length);

	for(var i = 0; i < data.length; ++i) {
		var row = data[i];
		row["email"] = local+delimiter+row.code+"@"+domain;
	}
}

function pendingEmailDataInit(data) {
	for(var i = 0; i < data.length; ++i) {
		var row = data[i];
		row["container"] = false;
		row["open"] = false;
		row["level"] = 0;
	}
	/*
	for(var i = 0; i < data.length; ++i) {
		var row = data[i];
		row["container"] = true;
		row["open"] = false;
		row["level"] = 0;
		row["subject"] = "";
	}
	*/
}

function parseXMLResponse(xmlResponse, options) {
	var data = new Array();
	var dataIndex = 0;
	var containerNode = xmlResponse.getElementsByTagName("container");
	if (containerNode.length > 0) {
		containerNode = containerNode.item(0);
		if (containerNode.hasChildNodes()) {
			for (var i = 0; i < containerNode.childNodes.length; ++i) {
				var node = containerNode.childNodes[i];
				if ((node.tagName) && (node.tagName == "row")) {
					var row = new Object();
					if (node.hasChildNodes()) {
						var childrenTwo = node.childNodes;
						for (var j = 0; j < childrenTwo.length; ++j) {
							var childTwo = childrenTwo[j];
							if (childTwo.tagName && (childTwo.tagName.length > 0)) {
								row[childTwo.tagName] = childTwo.textContent;
								if (options.email && (options.email == childTwo.tagName)) {
									row["username"] = childTwo.textContent.substring(0, childTwo.textContent.indexOf("@"));
									row["domain"] = childTwo.textContent.substring(childTwo.textContent.indexOf("@")+1, childTwo.textContent.length);
								}
							}
						}
					}
					data[dataIndex] = row;
					++dataIndex;
				}
			}
		}
	}
	return data;
}

function subjectLinesGlobalCallback(xmlResponse, options) {
	// lookup the treeview and call it's function
	var tree = globalTreeViews[options.treeId];
	tree.subjectLinesCallback(xmlResponse, options);
}

function pendingEmailTreeView() {
	var treeView = {
		rowCount: 0,
		data: null,
		spamScoreFilteredData: new Array(),
		spamScoreFilterValue: "",
		subjectCache: null,
		sortedColumn: null,
		sortedDirection: 0,

		// BEGIN FUNCTIONS
		cycleHeader: function cycleHeader(column) {
			/*
			if (column.id == "subject") {
				return;
			}
			*/
			// clear old column's sort
			if (this.sortedColumn) {
				this.sortedColumn.element.setAttribute("sortDirection", "");
			}

			// are we swapping an existing sorted column?
			if (column == this.sortedColumn) {
				if (this.sortedDirection == 0) {
					this.sortedDirection = 1;
				} else {
					this.sortedDirection = 0;
				}
			} else {
				this.sortedColumn = column;
				this.sortedDirection = 0;
			}
			this.sortData();
			document.getElementById("pendingEmail").treeBoxObject.invalidate();
		},
		getCellProperties: function getCellProperties(row,col,props){},
		getCellText: function getCellText(row,column){
			if (!this.data[row].hasOwnProperty(column.id)) {
				return "";
			} else {
				if (column.id == "date") {
					var seconds = Number(this.data[row][column.id]);
					var milliseconds = seconds * 1000;
					var d = new Date(milliseconds);
					var now = new Date();
					if ((d.getMonth() == now.getMonth()) && (d.getDate() == now.getDate())
						&& (d.getFullYear() == now.getFullYear())) {
						// if we are on the same day spit out the actual time
						return ((d.getHours() > 9) ? d.getHours() : ("0" + d.getHours())) + ":" +
						       ((d.getMinutes() > 9) ? d.getMinutes() : ("0" + d.getMinutes())) + ":" +
						       ((d.getSeconds() > 9) ? d.getSeconds() : ("0" + d.getSeconds()));
					} else {
						// else the full date
						return (d.getMonth()+1) + "-" + d.getDate() + "-" + d.getFullYear();
					}
				} else {
					return this.data[row][column.id];
				}
			}
		},
		getColumnProperties: function getColumnProperties(colid,col,props){},
		getImageSrc: function getImageSrc(row,col){ return null; },
		getLevel: function getLevel(row) { return this.data[row]["level"]; },
		getParentIndex: function getParentIndex(row) {
			if (this.data[row].container) {
				return -1;
			} else {
				for (var i = row; i >= 0; --i) {
					if (this.data[i].level < this.data[row].level) {
						return i;
					}
				}
			}
			return -1;
		},
		getRowProperties: function getRowProperties(row,props){},
		hasNextSibling: function hasNextSibling(row, afterIndex) {
			var curLevel = this.getLevel(row);
			if ((afterIndex+1) >= this.data.length) {
				return false;
			} else {
				var nextLevel = this.getLevel(afterIndex+1);
				if (nextLevel == curLevel) {
					return true;
				} else {
					return false;
				}
			}
		},
		isContainer: function isContainer(row) {
			if (!this.data[row].container) {
				var WTF = 1;
			}
			return this.data[row]["container"];
		},
		isContainerEmpty: function isContainerEmpty(row) { return false; },
		isContainerOpen: function isContainerOpen(row) { return this.data[row]["open"]; },
		isSeparator: function isSeparator(row){ return false; },
		isSorted: function isSorted(){ return false; },
		setData: function setData(data) { this.data = data; },
		setRowCount: function setRowCount(rowCount) { this.rowCount = rowCount; },
		setSpamScoreFilterValue: function setSpamScoreFilterValue(score) { this.spamScoreFilterValue = score; },
		setTree: function setTree(treebox){ this.treebox = treebox; },
		toggleOpenState: function toggleOpenState(row) {
			if (this.isContainerOpen(row)) {
				// how many children need closing?
				var i;
				for (i = 1; i < (this.data.length - row); ++i) {
					if (this.data[row+i]["level"] == 0) {
						break;
					}
				}
				// decrement i by one because we started at 1
				--i;

				this.data.splice(row+1, i);
				this.data[row]["open"] = false;
				this.rowCount = this.data.length;
				document.getElementById("pendingEmail").treeBoxObject.rowCountChanged(row+1, -i);
			} else {
				if (this.subjectCache && this.subjectCache[this.data[row]["sender"]]) {
					// scoot these rows into the main data..
					this.insertSubjectLines(this.subjectCache[this.data[row]["sender"]], this.data[row]["sender"]);
				} else {
					// go to the php and get me!
					var encodedData = "sender=" + encodeURIComponent(this.data[row]["sender"]);
					doemailRequest("getPendingEmailSubjectLines", encodedData, subjectLinesGlobalCallback, {treeId: "pendingEmail",
																																																	sender: this.data[row]["sender"]});
				}
			}
			document.getElementById("pendingEmail").treeBoxObject.invalidate();
		},
		insertSubjectLines: function insertSubjectLines(subjectLines, sender) {
			// check if we have a sorted date column so we can sort before inserting
			var sortProp = null;
			if (this.sortedColumn) {
				sortProp = this.sortedColumn.id;
			}
			function ascSort(a, b) {
				return (a[sortProp] < b[sortProp]) ? -1 : (a[sortProp] > b[sortProp]) ? 1 : 0;
			}

			function descSort(a, b) {
				return (a[sortProp] > b[sortProp]) ? -1 : (a[sortProp] < b[sortProp]) ? 1 : 0;
			}

			if ((sortProp != null) && (sortProp == "date")) {
				// sort the main list
				if (this.sortedDirection == 0) {
					subjectLines.sort(ascSort);
				} else {
					subjectLines.sort(descSort);
				}
			}

			// find where we need to insert
			var i;
			for (i = 0; i < this.data.length; ++i) {
				if (this.data[i]["sender"] == sender) {
					this.data[i]["open"] = true;
					break;
				}
			}
			for (var j = 0; j < subjectLines.length; ++j) {
				this.data.splice(i+1+j, 0, subjectLines[j]);
			}
			this.rowCount = this.data.length;
			document.getElementById("pendingEmail").treeBoxObject.rowCountChanged(i+1, subjectLines.length);
		},
		subjectLinesCallback: function subjectLinesCallback(subjectLines, options) {
			//var subjectLines = parseXMLResponse(xmlResponse, options);
			// add attributes to our data rows
			for(var i = 0; i < subjectLines.length; ++i) {
				var row = subjectLines[i];
				row["container"] = false;
				row["open"] = false;
				row["level"] = 1;
			}
			var sender = options.sender;
			// put these rows into the cache
			if (!this.subjectCache) {
				this.subjectCache = new Object();
			}
			this.subjectCache[sender] = subjectLines;
			this.insertSubjectLines(subjectLines, sender);
			document.getElementById("pendingEmail").treeBoxObject.invalidate();
		},
		// continue normal stuff
		sortData: function sortData() {
			var sortProp = this.sortedColumn.id;
			dump("sorting by: " + sortProp + "\n");
			function ascStringSort(a, b) {
				return (a[sortProp] < b[sortProp]) ? -1 : (a[sortProp] > b[sortProp]) ? 1 : 0;
			}

			function descStringSort(a, b) {
				return (a[sortProp] > b[sortProp]) ? -1 : (a[sortProp] < b[sortProp]) ? 1 : 0;
			}

			// we need to rip out any level 1 objects before sorting the primary list
			var subjectLines = new Array();
			var currentSender;
			// iterate through and add them to our backup list
			for (var i = 0; i < this.data.length; ++i) {
				if (this.data[i].level == 0) {
					currentSender = this.data[i].sender;
					subjectLines[currentSender] = new Array();
				} else {
					subjectLines[currentSender].push(this.data[i]);
				}
			}
			// now remove them from the list
			for (var i = 0; i < this.data.length; ++i) {
				while ((i < this.data.length) && (this.data[i].level == 1)) {
					this.data.splice(i, 1);
				}
			}

			// sort the main list
			if (this.sortedDirection == 0) {
				this.data.sort(ascStringSort);
				this.sortedColumn.element.setAttribute("sortDirection", "ascending");
			} else {
				this.data.sort(descStringSort);
				this.sortedColumn.element.setAttribute("sortDirection", "descending");
			}

			// if we are sorting date, sort all the individual arrays as well
			if (sortProp == "date") {
				for (entry in subjectLines) {
					if (this.sortedDirection == 0) {
						subjectLines[entry].sort(ascStringSort);
					} else {
						subjectLines[entry].sort(descStringSort);
					}
				}
			}

			// now reinsert all the data back in to the main list
			// go in reverse order for ease of use
			for (var i = this.data.length - 1; i >= 0; --i) {
				var sender = this.data[i].sender;
				if (sender && subjectLines[sender]) {
					for (var j = 0; j < subjectLines[sender].length; ++j) {
						this.data.splice(i+1+j, 0, subjectLines[sender][j]);
					}
				}
			}
		},
		filterData: function filterData() {
			dump("filterData called\n");
			var startLen = this.data.length;

			// Merge the main data and any existing filtered data
			if (this.spamScoreFilteredData != null) {
				this.data = this.data.concat(this.spamScoreFilteredData);
				this.spamScoreFilteredData = new Array();
			}

			// Check if we should filter
			if (this.spamScoreFilterValue != "") {
				var unfiltered = new Array();
				var filtered = new Array();

				function filterCallback(element, index, array) {
					if (element.hasOwnProperty("spamScore")) {
						if (element.spamScore >= this.spamScoreFilterValue) {
							filtered.push(element);
						} else {
							unfiltered.push(element);
						}
					} else {
						unfiltered.push(element);
					}
				}
				this.data.forEach(filterCallback, this);
				this.data = unfiltered;
				this.spamScoreFilteredData = filtered;
			}

			// update the count
			this.setRowCount(this.data.length);
			document.getElementById("pendingEmail").treeBoxObject.rowCountChanged(startLen-1, this.data.length - startLen);
			var strbundle = document.getElementById("doemailStringBundle");
			var countString = strbundle.getString("doemail.mainDialog.count.label");
			var label = document.getElementById("pendingEmailCountLabel");
			label.value = countString + " " + this.data.length;
			if (this.spamScoreFilteredData.length > 0) {
				label.value += " (" + this.spamScoreFilteredData.length + ")";
			}
		}
	};

	return treeView;
}

