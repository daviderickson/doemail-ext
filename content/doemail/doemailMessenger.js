var g_menuNames = {"mailContext":1,
										"emailAddressPopup":1,
										"input-box-contextmenu":1,
										"msgComposeContext":1,
										"doemailIconContext":1};

var doemailColumnHandler = {
	getCellText:         function(row, col) {
		//get the message's header so that we can extract the reply to field
		//var key = gDBView.getKeyAt(row);
		//var hdr = gDBView.db.GetMsgHdrForKey(key);

		//return hdr.getStringProperty("x-doemail");
	 return null;
	},
	getSortStringForRow: function(hdr) {return hdr.getStringProperty("x-doemail");},
	isString:            function() {return true;},

	getCellProperties:   function(row, col, props){},
	getRowProperties:    function(row, col, props){},
	getImageSrc:         function(row, col) {
		var key = gDBView.getKeyAt(row);
		var hdr = gDBView.db.GetMsgHdrForKey(key);
		var val = hdr.getStringProperty("x-doemail");
		//dump("Row:" + row + " key: " + key + " val: " + val + "\n");

		if (val.indexOf("white email") != -1) {
			return "chrome://doemail/content/images/greenlight-e.png"
		} else if (val.indexOf("white domain") != -1) {
			return "chrome://doemail/content/images/greenlight-d.png"
		} else if (val.indexOf("white code") != -1) {
			return "chrome://doemail/content/images/greenlight-c.png"
		} else if (val.indexOf("white to") != -1) {
			return "chrome://doemail/content/images/greenlight-l.png"
		} else if (val.indexOf("manually confirmed") != -1) {
			return "chrome://doemail/content/images/greenlight-e.png"
		} else if (val.indexOf("confirmed") != -1) {
			return "chrome://doemail/content/images/greenlight-ec.png"
		} else if (val.indexOf("black domain") != -1) {
			return "chrome://doemail/content/images/redlight-d.png"
		} else if (val.indexOf("black email") != -1) {
			return "chrome://doemail/content/images/redlight-e.png"
		} else if (val.indexOf("unknown") != -1) {
			return "chrome://doemail/content/images/yellowlight-question.png";
		}

		return null;
	},
	getSortLongForRow:   function(hdr) {return 0;}
}

var CreateDbObserver = {
  // Components.interfaces.nsIObserver
  observe: function(aMsgFolder, aTopic, aData)
  {
	  if (gDBView != null) {
			gDBView.addColumnHandler("colDOEmail", doemailColumnHandler);
		}
  }
}

/**
	Called when a menu with a DOEmail submenu is being shown, sends an RPC
	to the server to determine which menu options to present to the user
**/
function menuShowingEvent(event) {
	/* Check that this is a menu we are interested in, and its not a sub-menu event */
	if ((event.target.id == null) || !(event.target.id in g_menuNames)) {
		return;
	}

	// Find the DOEmail Menu
	var children = event.target.childNodes;
	var doemailMenu = null;
	for(var i=0; i < children.length; i++) {
		if ((children[i].id != null) && (children[i].id.indexOf("doemail") >= 0)) {
			doemailMenu = children[i];
			break;
		}
	}

	if (doemailMenu != null) {
		menuShowing(doemailMenu);
	}
}

/**
	Called from the textbox of a compose window
**/
function menuShowingEventAnon(event) {
	/* Check that this is a menu we are interested in, and its not a sub-menu event */
	if ((event.target.getAttribute("anonid") == null) || !(event.target.getAttribute("anonid") in g_menuNames)) {
		return;
	}

	// Find the DOEmail Menu
	var children = event.target.childNodes;
	var doemailMenu = null;
	for(var i=0; i < children.length; i++) {
		if ((children[i].getAttribute("anonid") != null) && (children[i].getAttribute("anonid").indexOf("doemail") >= 0)) {
			doemailMenu = children[i];
			break;
		}
	}

	if (doemailMenu != null) {
		menuShowing(doemailMenu);
	}
}

function menuShowing(doemailMenu) {
	// Ensure there is a non hidden menu separator ahead of us
	var curNode = doemailMenu.previousSibling;
	while ((curNode != null) && (curNode.hidden != null) && (curNode.hidden == true)) {
		curNode = curNode.previousSibling;
	}

	// if previous non hidden menu option is not a separator, unhide ours
	if ((curNode != null) && (curNode.nodeName != "menuseparator")) {
		doemailMenu.previousSibling.hidden = false;
	}

	// check if we are online or offline
	if (doemailIsOffline()) {
		doemailMenu.setAttribute("disabled", true);
		return;
	}

	/* Get the email address */
	var emailAddress = getSelectedText();

	var regexResults = emailRegex.exec(emailAddress);
	if (regexResults != null) {
		if (regexResults[0] != emailAddress) {
			emailAddress = regexResults[0];
		}

		/* setup a request to decide what we need to show in our menu */
		var encodedData="email=" + encodeURIComponent(json_encode([emailAddress]));
		var options = {"menu" : doemailMenu, "email" : emailAddress};
		doemailRequest("getMenuOptions", encodedData, menuShowingEventCallback, options);
		// disable the menu until the request returns
		doemailMenu.setAttribute("disabled", true);
	} else {
		// disable the menu
		doemailMenu.setAttribute("disabled", true);
	}
}

/**
	A callback called when the RPC returns with the menu options we want to show in our menu
	data contains multiple arrays:
    $retval["domainBlacklist"] = $domainBlacklist;
    $retval["domainBlacklistMiss"] = $domainBlacklistMiss;
    $retval["emailBlacklist"] = $emailBlacklist;
    $retval["emailWhitelist"] = $emailWhitelist;
    $retval["mailingListWhitelist"] = $mailingListWhitelist;
    $retval["domainWhitelist"] = $domainWhitelist;
    $retval["domainWhitelistMiss"] = $domainWhitelistMiss;

**/
function menuShowingEventCallback(data, options) {
	var doemailMenu = options.menu;

	/* Found Menu, iterate through its children building the resultant menus */
	var children = doemailMenu.childNodes[0].childNodes;
	for (var i=0; i < children.length; i++) {
		var child = children[i];
		var id = null;
		if (child.hasAttribute("anonid")) {
			id = child.getAttribute("anonid");
		} else if (child.id != null) {
			id = child.id;
		}

		if (id.indexOf("addWhitelist") >= 0) {
			// if blacklisted, or already fully whitelisted, hide
			if ((data.emailBlacklist.length > 0) ||
					(data.domainBlacklist.length > 0) ||
					((data.emailWhitelist.length > 0) && (data.domainWhitelistMiss.length == 0))
					) {

				child.hidden = true;
			} else {
				child.hidden = false;

				// build our menu items
				if (data.emailWhitelist.length == 0) {
					buildMenuItems(child.childNodes[0], [options.email], "addWhitelistEmail(['$arg'], genericStatusAndAlertCallback, {});");
				}
				buildMenuItems(child.childNodes[0], data.domainWhitelistMiss, "addWhitelistDomain(['$arg'], genericStatusAndAlertCallback, {});");
			}
		} else if (id.indexOf("removeWhitelist") >= 0) {
			// only show if this entry is already on the whitelist
			if ((data.emailWhitelist.length > 0) ||
					(data.domainWhitelist.length > 0)
					) {
				child.hidden = false;

				// build our menu items
				if (data.emailWhitelist.length > 0) {
					buildMenuItems(child.childNodes[0], data.emailWhitelist, "removeWhitelistEmail(['$arg'], genericStatusAndAlertCallback, {});");
				}
				buildMenuItems(child.childNodes[0], data.domainWhitelist, "removeWhitelistDomain(['$arg'], genericStatusAndAlertCallback, {});");
			} else {
				child.hidden = true;
			}
		} else if (id.indexOf("addMailinglist") >= 0) {
			if ((data.mailingListWhitelist.length > 0) ||
					(data.emailBlacklist.length > 0) ||
					(data.domainBlacklist.length > 0)) {

				child.hidden = true;

			} else {
				child.hidden = false;

				buildMenuItems(child.childNodes[0], [options.email], "addWhitelistTo(['$arg'], genericStatusAndAlertCallback, {});");
			}

		} else if (id.indexOf("removeMailinglist") >= 0) {
			// only show if this entry is already on the mailinglist whitelist
			if (data.mailingListWhitelist.length > 0) {
				child.hidden = false;
				buildMenuItems(child.childNodes[0], data.mailingListWhitelist, "removeWhitelistTo(['$arg'], genericStatusAndAlertCallback, {});");
			} else {
				child.hidden = true;
			}
		} else if (id.indexOf("addBlacklist") >= 0) {
			/* show if we are not fully blacklisted */
			if ((data.emailBlacklist.length > 0) && (data.domainBlacklistMiss.length == 0)) {
				child.hidden = true;
			} else {
				child.hidden = false;

				// build parameters for our menu function
				if (data.emailBlacklist.length == 0) {
					buildMenuItems(child.childNodes[0], [options.email], "addBlacklistEmail(['$arg'], genericStatusAndAlertCallback, {});");
				}
				buildMenuItems(child.childNodes[0], data.domainBlacklistMiss,
						"confirmCommand('doemail.confirm.blackdomain.title', 'doemail.confirm.blackdomain.body', ['$arg'], 'addBlacklistDomain([\"$arg\"], genericStatusAndAlertCallback, {})', '')");
			}
		} else if (id.indexOf("removeBlacklist") >= 0) {
			// only show if we have an entry on the blacklist
			if ((data.emailBlacklist.length > 0) ||
					(data.domainBlacklist.length > 0)
					) {
				child.hidden = false;

				// build parameters for our menu function
				if (data.emailBlacklist.length > 0) {
					buildMenuItems(child.childNodes[0], data.emailBlacklist, "removeBlacklistEmail(['$arg'], genericStatusAndAlertCallback, {});");
				}
				buildMenuItems(child.childNodes[0], data.domainBlacklist, "removeBlacklistDomain(['$arg'], genericStatusAndAlertCallback, {});");
			} else {
				child.hidden = true;
			}
		}
	}

	// enable the menu
	doemailMenu.setAttribute("disabled", false);

}

/**
	Called when a menu with a DOEmail submenu is being hidden,
	clears out all the submenus from our previous RPC
**/
function menuHidingEvent(event) {
	if ((event.target.id == null) || !(event.target.id in g_menuNames)) {
		return;
	}

	var popupMenu = document.getElementById(event.target.id);

	// Find the DOEmail Root Menu
	var children = popupMenu.childNodes;
	var doemailMenu = null;
	for(var i=0; i < children.length; i++) {
		if ((children[i].id != null) && (children[i].id.indexOf("doemail") >= 0)) {
			doemailMenu = children[i];
			break;
		}
	}

	if (doemailMenu != null) {
		menuHiding(doemailMenu);
	}
}

function menuHidingEventAnon(event) {
	/* Check that this is a menu we are interested in, and its not a sub-menu event */
	if ((event.currentTarget.getAttribute("anonid") == null) || !(event.currentTarget.getAttribute("anonid") in g_menuNames)) {
		return;
	}

	// Find the DOEmail Menu
	var children = event.target.childNodes;
	var doemailMenu = null;
	for(var i=0; i < children.length; i++) {
		if ((children[i].getAttribute("anonid") != null) && (children[i].getAttribute("anonid").indexOf("doemail") >= 0)) {
			doemailMenu = children[i];
			break;
		}
	}

	if (doemailMenu != null) {
		menuHiding(doemailMenu);
	}
}

function menuHiding(doemailMenu) {
	/* hide our menu separator */
	doemailMenu.previousSibling.hidden = true;

	var children = doemailMenu.childNodes[0].childNodes;
	for (var i=0; i < children.length; i++) {
		var child = children[i];
		var id = null;
		if (child.hasAttribute("anonid")) {
			id = child.getAttribute("anonid");
		} else {
			id = child.id;
		}

		if (id.indexOf("addWhitelist") >= 0) {
			clearMenuItems(child.childNodes[0]);
		} else if (id.indexOf("removeWhitelist") >= 0) {
			clearMenuItems(child.childNodes[0]);
		} else if (id.indexOf("addMailinglist") >= 0) {
			clearMenuItems(child.childNodes[0]);
		} else if (id.indexOf("removeMailinglist") >= 0) {
			clearMenuItems(child.childNodes[0]);
		} else if (id.indexOf("addBlacklist") >= 0) {
			clearMenuItems(child.childNodes[0]);
		} else if (id.indexOf("removeBlacklist") >= 0) {
			clearMenuItems(child.childNodes[0]);
		}
	}
}

function buildMenuItems(parent, dataArray, func) {
	for (i = 0; i < dataArray.length; ++i) {
		if (i in dataArray) {
			var node = document.createElement("menuitem");
			var cmd = func.replace(/\$arg/g, dataArray[i]); 
			node.setAttribute("label", dataArray[i]);
			node.setAttribute("oncommand", cmd);
			parent.appendChild(node);
		}
	}
}

function clearMenuItems(parent) {
	var children = parent.childNodes;
	for (var i = children.length - 1; i >= 0; --i) {
		parent.removeChild(children[i]);
	}
	parent.parentNode.hidden = true;
}

function folderSearchForAddresses() {
	var folderTree = document.getElementById("folderTree");
	var folderSelection = folderTree.view.selection;

	// This prevents a folder from being loaded in the case that the user
	// has right-clicked on a folder different from the one that was
	// originally highlighted.  On a right-click, the highlight (selection)
	// of a row will be different from the value of currentIndex, thus if
	// the currentIndex is not selected, it means the user right-clicked
	// and we don't want to load the contents of the folder.
	if (!folderSelection.isSelected(folderSelection.currentIndex)) {
		alert("You must first select (left click) this folder before searching it for addresses.");
		return;
	}

	var view = gDBView;
	var treeView = view.QueryInterface(Components.interfaces.nsITreeView);
	var count = treeView.rowCount;
	if (!count) {
		return;
	}

	var uriArray = Array();
	for (var i = 0; i < count; i++) {
		uriArray[i] = view.getURIForViewIndex(i);
	}

	window.openDialog("chrome://doemail/content/massImportWindow.xul","massImportWindow","chrome,resizable=yes,dialog=no", uriArray, messenger);
}

function onFolderPaneContextShowing(event) {
	var doemailMenu = document.getElementById("doemailFolderPaneContextMenu");

	// check if we need a separator above us
	var curNode = doemailMenu.previousSibling;
	while ((curNode != null) && (curNode.hidden != null) && (curNode.hidden == true)) {
		curNode = curNode.previousSibling;
	}

	// if previous non hidden menu option is not a separator, unhide ours
	if ((curNode != null) && (curNode.localName != "menuseparator")) {
		doemailMenu.previousSibling.hidden = false;
	}

	// check if we need a separator below us
	var curNode = doemailMenu.previousSibling;
	while ((curNode != null) && (curNode.hidden != null) && (curNode.hidden == true)) {
		curNode = curNode.nextSibling;
	}

	// if previous non hidden menu option is not a separator, unhide ours
	if ((curNode != null) && (curNode.localName != "menuseparator")) {
		doemailMenu.nextSibling.hidden = false;
	}

	doemailMenu.setAttribute("disabled", true);

	// check if we are online or offline
	if (doemailIsOffline()) {
		return;
	}

	var folderTree = document.getElementById("folderTree");
	var folderSelection = folderTree.view.selection;
	if (folderSelection.isSelected(folderSelection.currentIndex)) {
		var view = gDBView;
		var treeView = view.QueryInterface(Components.interfaces.nsITreeView);
		var count = treeView.rowCount;
		if (count) {
			// only enable if we are viewing the current selected index, and there are emails
			doemailMenu.setAttribute("disabled", false);
		}
	}
}

function onFolderPaneContextHiding(event) {
	var doemailMenu = document.getElementById("doemailFolderPaneContextMenu");
	doemailMenu.previousSibling.hidden = true;
	doemailMenu.nextSibling.hidden = true;
}

function confirmCommand(stringBundleTitleKey, stringBundleBodyKey, args, onSuccess, onFailure) {
	var strbundle = document.getElementById("doemailStringBundle");

	var ifps = Components.interfaces.nsIPromptService;

	var promptService = Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService();
	promptService = promptService.QueryInterface(ifps);

	var body = strbundle.getString(stringBundleBodyKey);
	body = body.replace(/\$arg0/g, args[0]);

	if (promptService && strbundle) {
		if (promptService.confirm(window,
					strbundle.getString(stringBundleTitleKey),
					body)) {
			eval(onSuccess);
		} else {
			eval(onFailure);
		}
	}
}

function doemailUpdatePendingEmailCount() {
	doemailRequest("getPendingEmailCount", "", doemailUpdatePendingEmailCountCallback, {});
}

function doemailUpdatePendingEmailCountCallback(data, options) {
	if ("object" == typeof(data)) {
		// we got an error object back from DOEmail, likely the user failed to authenticate
		return;
	}
	var strbundle = document.getElementById("doemailStringBundle");
	var button = document.getElementById("doemailToolbarButton");
	if (button) {
		button.setAttribute("label", strbundle.getString("doemail.button.label") + " (" + data + ")");
	}

	button = document.getElementById("doemailToolbarButtonOSX");
	if (button) {
		button.setAttribute("label", strbundle.getString("doemail.button.label") + " (" + data + ")");
	}
}
