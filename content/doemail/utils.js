function enableFieldsToggle(checkbox, arrayOfFieldNames) {
	var checked = false;
	if ((checkbox.localName == "checkbox") && (checkbox.checked)) {
		checked = true;
	}

	arrayOfFieldNames.forEach(function (element, index, array) {
		var field = document.getElementById(element);
		if (checked) {
			field.removeAttribute("disabled");
		} else {
			field.setAttribute("disabled", "true");
		}
	});
}